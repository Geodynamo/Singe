# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# General packages
import os,sys
sys.path.append(os.getcwd())

# Scientific packages
import numpy as np
import shtns

# Hand-made packages
import params


def set_shtns(sym, lmax, m):
	#-------------------------------------------------------------------
	# Init SHTNS params
	#-------------------------------------------------------------------
	#----------------
	# Truncation of spherical harmonics
	#	m even => Lmax odd
	#	m odd  => Lmax even
	#	m = 0  => Lmax even
	#----------------
	if (m%2 == 0) and (m > 0):
		if lmax%2 == 0:
			Lmax = lmax + 1
		else:
			Lmax = lmax
	elif (m%2 == 1) or (m == 0):
		if lmax%2 == 0:
			Lmax = lmax
		else:
			Lmax = lmax + 1
	
	#----------------
	# Choice of appropriate symmetry of pol/tor according to user choice
	#----------------
	if sym == 'pos':
		if m%2 == 0:
			ind_eig = 'neg'
		elif m%2 != 0:
			ind_eig = 'pos'
	elif sym == 'neg':
		if m%2 == 0:
			ind_eig = 'pos'
		elif m%2 != 0:
			ind_eig = 'neg'
	ind_eig_b = 'pos'
	
	#----------------
	# SHTNS init
	#----------------
	if m == 0:
		l0 = 1
		mmax_shtns = 0
		sh = shtns.sht(Lmax,mmax_shtns, 1)
	elif m > 0:
		l0 = m
		mmax_shtns = 1
		sh = shtns.sht(Lmax, mmax_shtns, m)
	sh.set_grid()

	return Lmax, ind_eig, ind_eig_b, sh
