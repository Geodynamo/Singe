# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np
from scipy.special import gamma

#-----------------------------------------------------------------------
# Radial grid generation
#-----------------------------------------------------------------------
class radial_grid():
#-----------------------------------------------------------------------
	"""
	Radial grid class.
	"""
#-----------------------------------------------------------------------
	def __init__(self, r0, rf, N, nin=0, nout=0):
	#-------------------------------------------------------------------
		"""
		Init
		"""
	#-------------------------------------------------------------------
		self.r0   = r0
		self.rf   = rf
		self.N    = N
		self.nin  = nin
		self.nout = nout
	
	def decre(self, N, y):
	#-------------------------------------------------------------------
		"""
		Auxiliary function.
		"""
	#-------------------------------------------------------------------
		e = y/(y+1)
		f = N+1
		x = e
		while np.abs(y + 1 - (1 - x**f)/(1-x) > 0.0001):
			x = e + (x**f)/(y+1)
		return x
	
	def mesh_reg(self):
	#-------------------------------------------------------------------
		"""Radial regular mesh.
		Inputs :
			* r0 : rayon initial [reel],
			* rf : rayon final [reel],
			* N  : nb d'intervalles [entier].
		Ouput : 
			* grid : classe de la grille reguliere.
		"""
	#-------------------------------------------------------------------
		self.r  = np.zeros((1, self.N+1))
		self.dr = np.zeros((1, self.N))
		self.h  = (self.rf - self.r0) / float(self.N)
		self.r[0,:] = self.r0 + self.h*np.arange(0, self.N+1, dtype=float)
		self.dr = self.h*np.ones((1, self.N), dtype=float)
	
	def mesh_irreg(self):
	#-------------------------------------------------------------------
		"""
		Irregular radial grid, following D. Jault et N. Schaeffer.
		Inputs :
			* r0   : rayon initial [reel],
			* rf   : rayon final [reel],
			* N    : nb d'intervalles [entier],
			* nin  : nb de points pour densifier la CL interne,
			* nout : nb de points pour densifier la CL externe.
		Ouput : 
			* grid : classe de la grille reguliere.
		"""
	#-------------------------------------------------------------------
		# Initialisation
		nb = self.N - (self.nin + self.nout)
		NG = 0
		NM = self.N
		nr1 = int(NG + (nb/4 + self.nin))
		nr2 = int(NM - (nb/4 + self.nout))
	
		# Verification
		if (self.nin + self.nout) >= self.N:
			print('nin+nout >= N')
			return
		if self.r0 == 0:
			nr1 = int(NG + (3*nb/20 + self.nin))
		if (nr1 >= nr2) or  (nr1 <= NG) or (nr2 >= NM):
			print('Switch grille reguliere')
			self.mesh_reg()
			return
	
		self.r  = np.zeros((1, self.N+1), dtype=float)
		self.dr = np.zeros((1, self.N), dtype=float)
	
		# Irregular grid generation
		# nr1 index of external shell of internal BL
		# nr2 index of internal shell of external BL
		h = (self.rf - self.r0)*0.15
		hu = (self.rf - self.r0 - 2*h)/(nr2 - nr1)
		self.r[0,NG]  = self.r0
		self.r[0,NM]  = self.rf
		self.r[0,nr1] = self.r0 + h
		self.r[0,nr2] = self.rf - h
		# Plateau uniforme dans le coeur
		for i in range(nr1+1, nr2):
			self.r[0,i] = self.r[0, nr1] + (i - nr1)*hu
		# Outer BL
		q = self.decre(NM-nr2, h/hu)
		e = hu
		for i in range(nr2+1, NM):
			e = e*q
			self.r[0,i] = self.r[0,i-1] + e
		# Inner BL
		q = self.decre(nr1-NG, h/hu)
		e = hu
		for i in range(nr1-1, NG, -1):
			e = e*q
			self.r[0,i] = self.r[0,i+1] - e
		# Radial path
		for k in range(0, self.N):
			self.dr[0,k] = self.r[0,k+1] - self.r[0,k]
		# Affectation
		self.dmin = self.dr.min()
		self.dmax = self.dr.max()

#-----------------------------------------------------------------------
# Finite differences
#-----------------------------------------------------------------------
def nufdwt(pts,order,ind,r):
#-----------------------------------------------------------------------
	"""Finite differences scheme on an irregular radial mesh.
	Inputs  :
		* pts   : nb of points for the finite difference scheme [integer],
		* order : order of the derivative to approximate [integer],
		* ind   : index of the radial point r[ind] to approximate the derivative,
		* r     : radial mesh.
	Output :
		* coeff : row vector of coefficients in the order u_{k-l},...,u_{k+l}
	"""
#-----------------------------------------------------------------------
	# Initialisation
	if order%2 == 1:		# Derivee impaire = +1 points
		pts = pts + 1
	M   = np.zeros((pts,pts),dtype=float)
	b   = np.zeros((pts,1),dtype=float)
	# Calcul des pas
	pas = np.zeros((1,pts),dtype=float)
	for i in range(0,pts):
		pas[0,i] = r[0,ind-pts//2+i]-r[0,ind]
	# Affectation
	b[order,0] = 1
	for i in range(0,pts):
		M[i,:] = pas**i / gamma(i+1)
	M[0,pts//2] = 1
	# Inversion
	coef = np.linalg.solve(M,b)
	# Mise en forme
	coef = coef.transpose()
	if pts == 3:
		dp1   = r[0,ind+1]-r[0,ind]
		dp2   = r[0,ind]-r[0,ind-1]
		if dp1 == dp2:			# Maille localement reguliere
			if order%2 == 1:		# Derivee impaire = +1 points
				coef[0,pts//2] = 0
	
#	# Theoretical formula (2d order) for benchmark
#	coef = np.zeros((1,3),dtype=float)
#	h1 = r[0,ind]   - r[0,ind-1]
#	h2 = r[0,ind+1] - r[0,ind]
#	if   order == 1:
#		coef[0,0] = -h2/((h1+h2)*h1)
#		coef[0,1] = (h2-h1)/(h1*h2)
#		coef[0,2] = h1/((h1+h2)*h2)
#	elif order == 2:
#		coef[0,0] = 2/(h1*(h1+h2))
#		coef[0,1] = -2/(h1*h2)
#		coef[0,2] = 2/(h2*(h1+h2))
	return coef

def mat_lapla(error,grid):
#-----------------------------------------------------------------------
	"""Remplissage du coeur de la matrice du Laplacien, sans le terme en l(l+1)/r^2.
	Inputs :
		* error : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid  : classe de la grille radiale.
	Output :
		* M     : matrice du Laplacien a (N-1)*(N-1) elements.
	"""
#-----------------------------------------------------------------------
	# Creation des vecteurs et matrices
	N = grid.N
	M = np.zeros((N-1,N-1),dtype=float)
	# Coeur de la matrice avec tous les coefficients
	nb_coef = error+1
	for i in range(error//2,N-error//2-2+1):
		coef1  = nufdwt(error,1,i+1,grid.r)
		coef2  = nufdwt(error+1,2,i+1,grid.r)
		ri     = grid.r[0,i+1]
		coef_M = coef2 + 2/ri * coef1
		M[i,i-error//2:i+error//2+1] = coef_M
	# Ordre 2 sur les bords pour le poloidal
		# En r1
	coef1_2 = nufdwt(2,1,1,grid.r)
	coef2_2 = nufdwt(3,2,1,grid.r)
	coef_M  = coef2_2 + 2/grid.r[0,1] * coef1_2
	M[0,0]  = coef_M[0,1]
	M[0,1]  = coef_M[0,2]
		# En rN-1
	coef1_2  = nufdwt(2,1,N-1,grid.r)
	coef2_2  = nufdwt(3,2,N-1,grid.r)
	coef_M   = coef2_2 + 2/grid.r[0,N-1] * coef1_2
	M[-1,-1] = coef_M[0,1]
	M[-1,-2] = coef_M[0,0]
	# Ordre croissant vers le centre
	k = 4
	i = 1
	while k <= error:
		nb_coef = k+1
		# CL vers r0
		coef1   = nufdwt(k,1,i+1,grid.r)
		coef2   = nufdwt(k+1,2,i+1,grid.r)
		coef_M  = coef2 + 2/grid.r[0,i+1] * coef1
		coef_M[0,nb_coef//2] = coef_M[0,nb_coef//2]
		M[i,0:nb_coef-1]     = coef_M[0,1:nb_coef]
		# CL vers rf
		coef1  = nufdwt(k,1,-(i+2),grid.r)
		coef2  = nufdwt(k+1,2,-(i+2),grid.r)
		coef_M = coef2 + 2/grid.r[0,-(i+2)] * coef1
		coef_M[0,nb_coef//2]    = coef_M[0,nb_coef//2]
		M[-(i+1),(-nb_coef+1):] = coef_M[0,0:nb_coef-1]
		k = k+2
		i = i+1
	return M

def mat_deriv1(error,grid):
#-----------------------------------------------------------------------
	"""Remplissage du coeur de la matrice de la derive premiere, utilie pour operateur L1 et L_{-1}.
	Inputs :
		* error : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid  : classe de la grille radiale,
	Output :
		* M     : matrice avec (N-1)*(N-1) elements.
	"""
#-----------------------------------------------------------------------
	# Initialisation
	N = grid.N
	M = np.zeros((N-1,N-1),dtype=float)
	# Coeur de la matrice avec tous les coefficients
	nb_coef = error+1
	for i in range(error//2,N-error//2-2+1):
		coef_M                       = nufdwt(error,1,i+1,grid.r)
		M[i,i-error//2:i+error//2+1] = coef_M
	# Ordre 2 sur les bords pour le poloidal
		# En r1
	coef_M = nufdwt(2,1,1,grid.r)
	M[0,0] = coef_M[0,1]
	M[0,1] = coef_M[0,2]
		# En rN-1
	coef_M   = nufdwt(2,1,N-1,grid.r)
	M[-1,-1] = coef_M[0,1]
	M[-1,-2] = coef_M[0,0]
	# Ordre croissant vers le centre
	k = 4
	i = 1
	while k <= error:
		nb_coef = k+1
		# CL vers r0
		coef_M   = nufdwt(k,1,i+1,grid.r)
		M[i,0:nb_coef-1]     = coef_M[0,1:nb_coef]
		# CL vers rf
		coef_M  = nufdwt(k,1,-(i+2),grid.r)
		M[-(i+1),(-nb_coef+1):] = coef_M[0,0:nb_coef-1]
		k = k+2
		i = i+1
	return M

def mat_bilapla_pol(Mat,grid,l,bc_i,bc_o):
#-----------------------------------------------------------------------
	""" Remplissage de la matrice de l'operateur biharmonique.
	Input :
		* Mat   : matrice du coeur de laplacien, sans l(l+1)/r^2, a (N-1) * (N-1) elements,
		* grid  : classe de la grille radiale,
		* l     : degre harmonique [entier],
		* bc_i  : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o  : 1 = FS a la CMB ; 2 = NS a la CMB.
	Output :
		* M     : matrice de l'operateur avec (N-1)*(N-1) elements.
	"""
#-----------------------------------------------------------------------
	# Creation des vecteurs et matrices
	N  = grid.N
	M  = Mat - (l*(l+1)) * np.diag(1/(grid.r[0,1:-1]**2))					 # (N-1) * (N-1)
	M1 = np.vstack([np.zeros((1,N-1),dtype=float),M,np.zeros((1,N-1),dtype=float)])# (N+1) * (N-1)
	M2 = np.hstack([np.zeros((N-1,1),dtype=float),M,np.zeros((N-1,1),dtype=float)])# (N-1) * (N+1)
	
	# BC a l'ICB ou au centre
	coef1_2   = nufdwt(2,1,1,grid.r)	# DF ordre 2 au bord pour la derivee 1
	coef2_2   = nufdwt(3,2,1,grid.r)	# DF ordre 2 au bord pour la derivee 2
	coef_M = coef2_2 + 2/grid.r[0,1] * coef1_2
	if   bc_i == 0:
		M1 = np.delete(M1,0,axis=0)
		M2 = np.delete(M2,0,axis=1)	# Suppresion de la premiere colonne
	elif bc_i == 1:
		k0      = 2/(grid.r[0,0]*grid.dr[0,0])
		M1[0,0] = k0
		M2[0,0] = coef_M[0,0]
	elif bc_i == 2:
		k0      = 2/(grid.dr[0,0]**2)
		M1[0,0] = k0
		M2[0,0] = coef_M[0,0]
	
	# BC a la CMB
	coef1_2   = nufdwt(2,1,N-1,grid.r)	# DF ordre 2 au bord pour la derivee 1
	coef2_2   = nufdwt(3,2,N-1,grid.r)	# DF ordre 2 au bord pour la derivee 2
	coef_M    = coef2_2 + 2/grid.r[0,N-1] * coef1_2
	M2[-1,-1] = coef_M[0,2]
	if   bc_o == 1:
		kN        = -2/(grid.r[0,-1]*grid.dr[0,-1])
		M1[-1,-1] = kN
	elif bc_o == 2:
		kN        = 2/(grid.dr[0,-1]**2)
		M1[-1,-1] = kN
	
	M = np.dot(M2,M1)
	return M

def mat_lapla_tor(Mat,grid,l,bc_i,bc_o):
#-----------------------------------------------------------------------
	"""Remplissage de la matrice du Laplacien pour le toroidal;
	Inputs :
		* Mat  : matrice du coeur de laplacien, sans l(l+1)/r^2, a (N-1) * (N-1) elements,
		* grid : classe de la grille radiale,
		* l    : degre harmonique [entier],
		* bc_i : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o : 1 = FS a la CMB ; 2 = NS a la CMB.
	Output :
		* M     : matrice du Laplacien, avec nb d'elements compris entre (N-1)^2 et (N+1)^2.
	"""
#-----------------------------------------------------------------------
	# Creation des vecteurs et matrices
	N  = grid.N
	M  = Mat - (l*(l+1)) * np.diag(1/(grid.r[0,1:-1]**2))	# (N-1) * (N-1)
	# BC a l'ICB
	if bc_i == 1:
		M = np.vstack([np.zeros((1,N-1),dtype=float),M])
		M = np.hstack([np.zeros((N,1),dtype=float),M])
		# Lapla en r0
		h       = grid.dr[0,0]
		M[0,0]  = 2/(grid.r[0,0]**2) - 2/h**2 - 2/(h*grid.r[0,0]) - l*(l+1)/(grid.r[0,0]**2)
		M[0,1]  = 2/h**2
		# Coeff manquant en r1
		coef1   = nufdwt(2,1,1,grid.r)
		coef2   = nufdwt(3,2,1,grid.r)
		coef_M  = coef2 + 2/grid.r[0,1] * coef1
		M[1,0]  = coef_M[0,0]
	# BC a la CMB
	if bc_o == 1:
		nb_l,nb_col = M.shape
		M = np.vstack([M,np.zeros((1,nb_col),dtype=float)])
		M = np.hstack([M,np.zeros((nb_l+1,1),dtype=float)])
		# Lapla en rN
		h        = grid.dr[0,-1]
		M[-1,-1] = 2/(grid.r[0,-1]**2) - 2/h**2 + 2/(h*grid.r[0,-1]) - l*(l+1)/(grid.r[0,-1]**2)
		M[-1,-2] = 2/h**2
		# Coeff manquant en rN-1
		coef1   = nufdwt(2,1,N-1,grid.r)
		coef2   = nufdwt(3,2,N-1,grid.r)
		coef_M   = coef2 + 2/grid.r[0,N-1] * coef1
		M[-2,-1] = coef_M[0,2]
	return M

#-----------------------------------------------------------------------
# Matrice de la temperature
#-----------------------------------------------------------------------
def mat_lapla_temp(Mat,grid,l,bc_i_temp,bc_o_temp):
#-----------------------------------------------------------------------
	"""Remplissage de la matrice du Laplacien pour la temperature.
	Inputs :
		* Mat       : matrice du coeur de laplacien, sans l(l+1)/r^2, a (N-1) * (N-1) elements,
		* grid      : classe de la grille radiale,
		* l         : degre harmonique > 0,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T imposee nulle a l'ICB,
		* bc_o_temp : 1 = Flux impose a la CMB ; 2 = T imposee a la CMB.
	Output :
		* M     : matrice du Laplacien dont le nb d'elements depend des BC.
	"""
#-----------------------------------------------------------------------
	# Creation des vecteurs et matrices
	N = grid.N
	M  = Mat - (l*(l+1)) * np.diag(1/(grid.r[0,1:-1]**2))	# (N-1) * (N-1)
	# BC a l'ICB
	if bc_i_temp == 1:
		M = np.vstack([np.zeros((1,N-1),dtype=float),M])
		M = np.hstack([np.zeros((N,1),dtype=float),M])
		# Lapla en r0
		h      = grid.dr[0,0]
		M[0,0] = -2/h**2 - l*(l+1)/(grid.r[0,0]**2)
		M[0,1] = 2/h**2
		# Coeff manquant en r1
		coef1  = nufdwt(2,1,1,grid.r)
		coef2  = nufdwt(3,2,1,grid.r)
		coef_M = coef2 + 2/grid.r[0,1] * coef1
		M[1,0] = coef_M[0,0]
	# BC a la CMB
	if bc_o_temp == 1:
		nb_l,nb_col = M.shape
		M = np.vstack([M,np.zeros((1,nb_col),dtype=float)])
		M = np.hstack([M,np.zeros((nb_l+1,1),dtype=float)])
		# Lapla en rN
		h        = grid.dr[0,-1]
		M[-1,-1] = -2/h**2 - l*(l+1)/(grid.r[0,-1]**2)
		M[-1,-2] = 2/h**2
		# Coeff manquant en rN-1
		coef1    = nufdwt(2,1,N-1,grid.r)
		coef2    = nufdwt(3,2,N-1,grid.r)
		coef_M   = coef2 + 2/grid.r[0,N-1] * coef1
		M[-2,-1] = coef_M[0,2]
	return M

def mat_L_pol(Mat, grid, l, couple, coeff_spec1, coeff_spec2, bc_i, bc_o):
#-----------------------------------------------------------------------
	"""Remplissage de la matrice des operateurs L1 et L_{-1} pour le poloidal.
	Inputs :
		* Mat         : matrice de la derivee premiere, avec (N-1) * (N-1) elements,
		* grid        : classe de la grille radiale,
		* l           : degre harmonique de l'equation [entier],
		* couple      : indice pour choisir l'operateur. 1 = L1 et -1 = L_{-1},
		* coeff_spec1 : coeff spectral du terme 1/r,
		* coeff_spec2 : coeff spectral du terme d/dr,
		* bc_i        : 0 = BC en r=0 ; 1 : FS a l'ICB ; 2 : NS a l'ICB,
		* bc_o        : 1 = FS a la CMB ; 2 = NS a la CMB.
	Output :
		* M           : avec nb d'elements compris entre (N-1)^2 et (N+1)^2.
	"""
#-----------------------------------------------------------------------
	# Spectral coefficient of the Q3 operator
	if   couple == 1:
		a = l*(l+2)  * coeff_spec1
		b = -l*(l+2) * coeff_spec2
	elif couple == -1:
		a = (l**2-1) * coeff_spec1
		b = (1-l**2) * coeff_spec2
	
	# Init
	N = grid.N
	M = b*Mat + a*np.diag(1/(grid.r[0,1:-1]))
	
	# BC a l'ICB
	if bc_i == 1:
		M = np.vstack([np.zeros((1, N-1),dtype=float),M])
		M[0,0] = b/grid.dr[0,0]
	# BC a la CMB
	if bc_o == 1:
		M = np.vstack([M,np.zeros((1,N-1),dtype=float)])
		M[-1,-1] = -b/grid.dr[0,-1]
	return M

def mat_L_tor(Mat, grid, l, couple, coeff_spec1, coeff_spec2, bc_i, bc_o):
#-----------------------------------------------------------------------
	"""Remplissage de la matrice des operateurs L1 et L_{-1} pour le toroidal.
	Inputs :
		* Mat         : matrice de la derivee premiere, avec (N-1) * (N-1) elements,
		* grid        : classe de la grille radiale,
		* l           : degre harmonique de l'equation [entier],
		* couple      : indice pour choisir l'operateur. 1 = L1 et -1 = L_{-1},
		* coeff_spec1 : coeff spectral du terme 1/r,
		* coeff_spec2 : coeff spectral du terme d/dr,
		* bc_i        : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o        : 1 = FS a la CMB ; 2 = NS a la CMB.
	Output :
		* M           : matrice (N-1) * (N-1) a (N+1)
	"""
#-----------------------------------------------------------------------
	# Coefficients spectraux
	if   couple == 1:
		a = l*(l+2)  * coeff_spec1
		b = -l*(l+2) * coeff_spec2
	elif couple == -1:
		a = (l**2-1) * coeff_spec1
		b = (1-l**2) * coeff_spec2
	
	# Initialisation
	N = grid.N
	M = b*Mat + a*np.diag(1/(grid.r[0,1:-1]))
	
	# Boundary conditions
	if bc_i == 1:
		# Missing coefficient at r = r1
		M      = np.hstack([np.zeros((N-1,1),dtype=float),M])
		coef1  = nufdwt(2,1,1,grid.r)
		coef_M = b * coef1
		M[0,0] = coef_M[0,0]
	
	if bc_o == 1:
		# Missing coefficient at r = rN-1
		M = np.hstack([M,np.zeros((N-1,1),dtype=float)])
		coef1    = nufdwt(2,1,N-1,grid.r)
		coef_M   = b * coef1
		M[-1,-1] = coef_M[0,2]
	
	return M
