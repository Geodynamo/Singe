# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# General packages
import os,sys,shutil
sys.path.append(os.getcwd())
import time

# Scientific packages
import numpy as np
from math import pi
import scipy.sparse as sc_sparse
import scipy.io as IO
from mpi4py import MPI

# Hand-made packages
import sub_op_irreg as sub_op
import sub_eig_irreg as sub_eig
import sub_init_spat as init
import params

#### avoid deprecation warnings in pyhton 3.3+
try: 
	time.clock = time.process_time   # time.clock is deprecated, should be replaced by time.process_time if available
except:
	pass

#-----------------------------------------------------------------------
# Init
#-----------------------------------------------------------------------
CWD = os.getcwd()
abs_path_singe = os.path.dirname(os.path.realpath(__file__))
comm = MPI.COMM_WORLD
mpi_rank = comm.Get_rank()
mpi_size = comm.Get_size()

if mpi_rank == 0:
	print("""\n########################################################
\tWelcome in SINGE: LINEAR CONVECTION
########################################################
""")

comm.Barrier()

#-----------------------------------------------------------------------
# Radial grid
#-----------------------------------------------------------------------
if params.reg == 'reg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N)
	grid.mesh_reg()
elif params.reg == 'irreg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N, nin=params.nin, nout=params.nout)
	grid.mesh_irreg()

if mpi_rank == 0:
	print('o Radial grid')
	if params.reg == 'reg':
		print('\t* h = ' + str(grid.h))
	elif params.reg == 'irreg':
		print('\t* N = ' + str(grid.N))
		print('\t* hmin = ' + str(grid.dmin))
		print('\t* hmax = '+ str(grid.dmax))

comm.Barrier()

#-----------------------------------------------------------------------
# Radial fields
#-----------------------------------------------------------------------
N2 = params.N2r(grid.r)
gr = params.gravity(grid.r)

if np.any(params.Rac):								# Can be single value or a list
	flag_temp = 2
	N2c = params.N2cr(grid.r)											# Radial field
	bc_i_chi = params.bc_i_chi
	bc_o_chi = params.bc_o_chi
else:
	flag_temp = 1
	N2c = np.zeros(np.size(grid.r))
	bc_i_chi = 1														# Default value. Does not play any role because composition is disabled!
	bc_o_chi = 1														# Idem

#-----------------------------------------------------------------------
# Creating bulk matrices in parallel (without Ra)
#-----------------------------------------------------------------------
mc_all = np.array(params.m)
par_idx0 = mpi_rank * np.size(mc_all) // mpi_size
par_idx1 = (mpi_rank+1) * np.size(mc_all) // mpi_size
mc_vec = mc_all.flat[par_idx0:par_idx1]			# m

count = 0

if mpi_rank == 0:
	print('\no Building the A and B matrices on ' + str(mpi_size) + ' MPI process')

comm.Barrier()

for m in mc_vec:
	#----------------
	# Init
	#----------------
	path_mdir = 'm' + str(m)
	if os.path.isdir(path_mdir) == False:
		os.mkdir(path_mdir)
	shutil.copy('params.py', path_mdir)
	os.chdir(path_mdir)
	
	#----------------
	# Spectral scalar variables: same resolution for each (m, parity)
	#----------------
	Lmax, ind_eig, ind_eig_b, sh = sub_eig.set_shtns(params.sym, params.Lmax, m.item())
	pol, tor, temp, chi, polb, torb = sub_eig.nb_eig_vec(m, params.N, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi)
	
	#----------------
	# Modify params.py for graph
	#----------------
	with open("params.py", "a") as myfile:
		myfile.write("\nm = " + str(m))
		myfile.write("\nLmax = " + str(Lmax))
		myfile.write("\nRa = %.3e" %(params.Ra))
		if flag_temp == 2:
			myfile.write("\nRac = %.3e" %(params.Ra))	# Just needed for plots

	#----------------
	# Size of the matrices A and B
	#----------------
	if flag_temp == 1:
		nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
	elif flag_temp == 2:
		nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec + chi.nbel_vec
	
	#----------------
	# Eigenvalue matrices AX = lambda B X
	#----------------
	# Filling of B
	tic = time.clock()
	B = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)
	B = sub_eig.hydro_eig_B(B, params.error, grid, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, flag_temp)
	B = sub_eig.temp_eig_B(B, params.error, grid, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, flag_temp)

	# Filling of A
	A = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)
	A = sub_eig.hydro_eig_A(A, params.error, grid, sh, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, params.Omega0, params.nu, flag_temp)
	A = sub_eig.temp_eig_A(A, params.error, grid, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, params.kappa, params.kappac, 0, 0, N2, N2c, gr, flag_temp)

	# Save of on the HDD
	A = sc_sparse.csr_matrix(A)
	B = sc_sparse.csr_matrix(B)
	IO.mmwrite('B.mtx', B)
	IO.mmwrite('A_withoutRa.mtx', A)
	toc = time.clock()
	timeMat = toc - tic

	# Check for errors:
	if not all(np.isfinite(A.data)):
		print('Error in matrix A, inf/nan found')
	if not all(np.isfinite(B.data)):
		print('Error in matrix B, inf/nan found')
	del A, B
	print('\tx m = ' + str(m) + ': ' + str(round(timeMat,2)) + ' s')
	os.chdir(CWD)

comm.Barrier()

if (mpi_rank == 0) and (params.reg == 'irreg'):
	title_grid = 'Params_grid_Ek' + str(params.Ek) + '_Pr' + str(params.Pr) + 'grid' + str(params.reg) + '_N' + str(params.N) + '_L' + str(params.Lmax) + '_BCI' + str(params.bc_i) + '_BCO' + str(params.bc_o) + '_BCTI' + str(params.bc_i_temp) + '_BCTO' + str(params.bc_o_temp) + '_BCCHI' + str(bc_i_chi) + '_BCCHO' + str(bc_o_chi) + '_sym' + str(params.sym) + '.txt'
	txt3 = open(title_grid,'w')
	txt3.write('dmin = %f\n' %(grid.dmin))
	txt3.write('dmax = %f\n' %(grid.dmax))
	txt3.write('nin  = %f\n' %(params.nin))
	txt3.write('nout = %f\n' %(params.nout))
	txt3.close()
