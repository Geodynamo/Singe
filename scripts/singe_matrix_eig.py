# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# General packages
import os,sys
sys.path.append(os.getcwd())
import time

# Scientific packages
import numpy as np
from math import pi
import scipy.sparse as sc_sparse
import scipy.io as IO
import shtns

# Hand-made packages
import sub_op_irreg as sub_op
import sub_eig_irreg as sub_eig
import sub_init_spat as init
import params

#### avoid deprecation warnings in pyhton 3.3+
try: 
	time.clock = time.process_time   # time.clock is deprecated, should be replaced by time.process_time if available
except:
	pass

print("""\n####################################################
\tWelcome in SINGE: EIGENMODE MATRICES
####################################################
""")

#-----------------------------------------------------------------------
# Radial grid
#-----------------------------------------------------------------------
print('o Radial grid')
if params.reg == 'reg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N)
	grid.mesh_reg()
elif params.reg == 'irreg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N, nin=params.nin, nout=params.nout)
	grid.mesh_irreg()

if params.reg == 'reg':
	print('\t* h = ' + str(grid.h[0]))
elif params.reg == 'irreg':
	print('\t* N = ' + str(grid.N))
	print('\t* hmin = ' + str(grid.dmin))
	print('\t* hmax = '+ str(grid.dmax))

#-----------------------------------------------------------------------
# Disabling buoyancy or not
#-----------------------------------------------------------------------
if params.Ra != 0:
	# Radial field
	N2 = params.N2r(grid.r)
	gr = params.gravity(grid.r)
	# BC
	bc_i_temp = params.bc_i_temp
	bc_o_temp = params.bc_o_temp
	if params.Rac != 0:
		# Radial field
		N2c = params.N2cr(grid.r)
		# BC
		bc_i_chi = params.bc_i_chi
		bc_o_chi = params.bc_o_chi
	else:
		N2c = np.zeros(np.size(grid.r))
		bc_i_chi = 1	# Default value. Does not play any role because composition is disabled!
		bc_o_chi = 1	# Idem
else:
	N2, N2c = np.zeros(np.size(grid.r)), np.zeros(np.size(grid.r))
	bc_i_temp = 1	# Default value. Does not play any role because temperature is disabled!
	bc_o_temp = 1	# Idem
	bc_i_chi = 1	# Default value. Does not play any role because composition is disabled!
	bc_o_chi = 1	# Idem

#-----------------------------------------------------------------------
# Parameters
#-----------------------------------------------------------------------
# Lmax, ind_eig, ind_eig_b, sh = sub_eig.set_shtns(params.sym, params.Lmax, params.m, params.m)
Lmax, ind_eig, ind_eig_b, sh = sub_eig.set_shtns(params.sym, params.Lmax, params.m)
pol, tor, temp, chi, polb, torb = sub_eig.nb_eig_vec(params.m, params.N, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)

if params.Ra != 0:
	if params.Rac != 0:
		print('\no Chemical and thermal parts enabled')
		flag_temp = 2
		nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec + chi.nbel_vec
	else:
		print('\no Only thermal part enabled')
		flag_temp = 1
		nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
else:
	print('\no Buoyancy disabled')
	flag_temp = 0
	nb_l = pol.nbel_vec + tor.nbel_vec

A = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)
B = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)

#-----------------------------------------------------------------------
# Filling of B
#-----------------------------------------------------------------------
ticB = time.clock()

B = sub_eig.hydro_eig_B(B, params.error, grid, params.m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, flag_temp)
if params.Ra != 0:
	B = sub_eig.temp_eig_B(B, params.error, grid, params.m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, flag_temp)
tocB = time.clock()

print('\tMatrix B done in ' + str(tocB - ticB) + ' s')

#-----------------------------------------------------------------------
# Filling of A
#-----------------------------------------------------------------------
ticA = time.clock()

A = sub_eig.hydro_eig_A(A, params.error, grid, sh, params.m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, params.Omega0, params.nu, flag_temp)
if params.Ra != 0:
	A = sub_eig.temp_eig_A(A, params.error, grid, params.m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, params.kappa, params.kappac, params.Ra, params.Rac, N2, N2c, gr, flag_temp)

tocA = time.clock()
print('\tMatrix A done in ' + str(tocA - ticA) + ' s')

#-----------------------------------------------------------------------
# Save of sparse matrix
#-----------------------------------------------------------------------
A = sc_sparse.csr_matrix(A)
IO.mmwrite('A.mtx', A)
del A

B = sc_sparse.csr_matrix(B)
IO.mmwrite('B.mtx', B)
del B

print('\no A and B saved in the working directory')
