# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import sys, os, time
sys.path.append(os.getcwd())

import numpy as np
import scipy.sparse as sc_sparse
import scipy.io as IO
import params

import slepc4py
slepc4py.init(sys.argv + [str(params.eig)] + [str(params.tau[0]), str(params.tau[1]) + '+' + str(params.tau[2]) + 'i'])

from petsc4py import PETSc
from slepc4py import SLEPc

#### avoid deprecation warnings in pyhton 3.3+
try: 
	time.clock = time.process_time   # time.clock is deprecated, should be replaced by time.process_time if available
except:
	pass

#-----------------------------------------------------------------------
# Parameters for PETSc / SLEPc
#-----------------------------------------------------------------------
slepc4py.init(sys.argv)
Print = PETSc.Sys.Print
rank = PETSc.COMM_WORLD.getRank()
size = PETSc.COMM_WORLD.getSize()
opts = PETSc.Options()
#rank = PETSc.COMM_SELF.getRank()
#size = PETSc.COMM_SELF.getSize()

## For mumps to work better (allow to use more memory)
opts["mat_mumps_icntl_14"] = 80

#-----------------------------------------------------------------------
# Solver SLEPc
#-----------------------------------------------------------------------
def solv_eig_syst(A, B, nev, ncv, tol=10**(-8), maxit=100):
#-----------------------------------------------------------------------
	"""Call of SLEPc solver for the GEP AX = kBX.
	Inputs : 
		* A      : sparse matrix N*N PETSc,
		* B      : sparse matrix N*N PETSc,
		* nev    : nb of eigenvalues to compute at minimum [integer],
		* ncv    : size of the Krylov space to use for the eigenvalue comptuations ncv >> 2 nev for accurate computations,
		* tol    : tolerance [default = 10**(-8)],
		* mpd    : maximum projected dimension [default = 0],
	Output : 
		* sol    : class.
	"""
#-----------------------------------------------------------------------
	# Initialisation
	class solution:
		"Definition de la classe solution"
	sol  = solution()
	
	# Solver E
	E = SLEPc.EPS()
	E.create(SLEPc.COMM_WORLD)
	E.setOperators(A,B)
	E.setProblemType(SLEPc.EPS.ProblemType.GNHEP)
	E.setDimensions(nev,ncv)
	E.setTolerances(tol,maxit)
	
	if params.eig == '-eps_largest_magnitude':
		E.setWhichEigenpairs(SLEPc.EPS.Which.LARGEST_MAGNITUDE)
	elif params.eig == '-eps_smallest_magnitude':
		E.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_MAGNITUDE)
	elif params.eig == '-eps_largest_real':
		E.setWhichEigenpairs(SLEPc.EPS.Which.LARGEST_REAL)
	elif params.eig == '-eps_smallest_real':
		E.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_REAL)
	elif params.eig == '-eps_largest_imaginary':
		E.setWhichEigenpairs(SLEPc.EPS.Which.LARGEST_IMAGINARY)
	elif params.eig == '-eps_smallest_imaginary':
		E.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_IMAGINARY)
	else:
		if params.eig == '-eps_target_magnitude':
			E.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_MAGNITUDE)
		elif params.eig == '-eps_target_real':
			E.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_REAL)
		elif params.eig == '-eps_target_imaginary':
			E.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_IMAGINARY)
		E.setTarget(float(params.tau[1])+1j*float(params.tau[2]))
	
	# Solving
	E.setFromOptions()
	tic = time.clock()
	E.solve()
	toc = time.clock()
	
	# Getting the outputs
	sol.its                   = E.getIterationNumber()
	sol.neps_type             = E.getType()
	sol.tol, sol.maxit        = E.getTolerances()
	sol.nev, sol.ncv, sol.mpd = E.getDimensions()
	sol.nconv                 = E.getConverged()
	sol.k                     = np.zeros((1,sol.nconv),dtype=complex)
	#~ sol.error                 = np.zeros((1,sol.nconv))
	sol.vec                   = np.zeros((nb_l,sol.nconv),dtype=complex)
	sol.tau                   = E.getTarget()
	if sol.nconv > 0:
		# Initialisation des vecteurs propres
		vr, wr = A.getVecs()
		vi, wi = A.getVecs()
		for i in range(0,sol.nconv):
			# Creation des valeurs et mise en commun sur le processeur 0
			k     = E.getEigenpair(i, vr,vi)
#			error = E.computeRelativeError(i)
			tozero,VR = PETSc.Scatter.toZero(vr)
			tozero.begin(vr,VR)
			tozero.end(vr,VR)
			tozero.destroy()
			# Affectation
			sol.k[0,i]     = k
#			sol.error[0,i] = error
			if rank == 0:
				sol.vec[0:,i] = VR[0:]
				sol.time      = toc-tic
	return sol

#-----------------------------------------------------------------------
# PETSc Matrix B
#-----------------------------------------------------------------------
# Import of CSR sparse matrix 
B = IO.mmread('B.mtx')
B = sc_sparse.csr_matrix(B)
nb_l,nb_c = B.shape
nbl = opts.getInt('nbl',nb_l)

# Fast PETSc conversion
MB = PETSc.Mat()
MB.create(PETSc.COMM_WORLD)
MB.setSizes([nbl,nbl])
# #try:
MB.setType('mpiaij')
# #except:
	# #MB.setType('aij')
MB.setFromOptions()
MB.setUp()

Istart,Iend = MB.getOwnershipRange()
indptrB = B[Istart:Iend,:].indptr
indicesB = B[Istart:Iend,:].indices
dataB = B[Istart:Iend,:].data

del B

MB.setPreallocationCSR(csr=(indptrB,indicesB))
MB.setValuesCSR(indptrB,indicesB,dataB)

MB.assemblyBegin()
MB.assemblyEnd()

del indptrB,indicesB,dataB

#-----------------------------------------------------------------------
# PETSc Matrix A
#-----------------------------------------------------------------------
# Import of CSR non-zero elements
A = IO.mmread('A.mtx')
A = sc_sparse.csr_matrix(A)

# Fast PETSc conversion
MA = PETSc.Mat()
MA.create(PETSc.COMM_WORLD)
MA.setSizes([nbl,nbl])
# #try:
MA.setType('mpiaij')
# #except:
	# #MA.setType('aij')	# Some solvers do not handle mpi matrices
MA.setFromOptions()
MA.setUp()

Istart,Iend = MA.getOwnershipRange()
indptrA = A[Istart:Iend,:].indptr
indicesA = A[Istart:Iend,:].indices
dataA = A[Istart:Iend,:].data

del A

MA.setPreallocationCSR(csr=(indptrA,indicesA))
MA.setValuesCSR(indptrA,indicesA,dataA)

MA.assemblyBegin()
MA.assemblyEnd()

del indptrA,indicesA,dataA

#-----------------------------------------------------------------------
# Solver call
#-----------------------------------------------------------------------
sol = solv_eig_syst(MA, MB, params.nev, params.ncv, maxit=params.maxit, tol=params.tol)

#-----------------------------------------------------------------------
# Save of eigenmodes and parameters simulations
#-----------------------------------------------------------------------
if rank == 0:
	# Eigenvalues
	txt1 = np.hstack([np.real(sol.k).transpose(),np.imag(sol.k).transpose()])#,sol.error.transpose()])
	title_txt1 = 'Eigenval.txt'
	np.savetxt(title_txt1,txt1)
	# Eigenvectors
	title_txt21 = 'Real_Eigenvec.npy'
	title_txt22 = 'Imag_Eigenvec.npy'
	np.save(title_txt21,np.real(sol.vec))
	np.save(title_txt22,np.imag(sol.vec))
