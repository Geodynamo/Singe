# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np
import sub_op_irreg as sub
import shtns

#-------------------------------------------------------------------
# Init SHTNS params
#-------------------------------------------------------------------
def set_shtns(sym, lmax, m):
	#----------------
	# Truncation of spherical harmonics
	#	m even => Lmax odd
	#	m odd  => Lmax even
	#	m = 0  => Lmax even
	#----------------
	if (m%2 == 0) and (m > 0):
		if lmax%2 == 0:
			Lmax = lmax + 1
		else:
			Lmax = lmax
	elif (m%2 == 1) or (m == 0):
		if lmax%2 == 0:
			Lmax = lmax
		else:
			Lmax = lmax + 1
	
	#----------------
	# Choice of appropriate symmetry of pol/tor according to user choice
	#----------------
	if sym == 'pos':
		if m%2 == 0:
			ind_eig = 'neg'
		elif m%2 != 0:
			ind_eig = 'pos'
	elif sym == 'neg':
		if m%2 == 0:
			ind_eig = 'pos'
		elif m%2 != 0:
			ind_eig = 'neg'
	ind_eig_b = 'pos'
	
	#----------------
	# SHTNS init
	#----------------
	if m == 0:
		l0 = 1
		mmax_shtns = 0
		sh = shtns.sht(Lmax,mmax_shtns, 1)
	elif m > 0:
		l0 = m
		mmax_shtns = 1
		sh = shtns.sht(Lmax, mmax_shtns, m)
	sh.set_grid()

	return Lmax, ind_eig, ind_eig_b, sh

#-----------------------------------------------------------------------
# Useful functions to determine the size of the matrices
#-----------------------------------------------------------------------
def nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi):
	#-------------------------------------------------------------------
	""" Retourne diverses informations concernant le vecteur propre pour les trois scalaires.
	Inputs :
		* m         : ordre azimuthal du mode,
		* N         : nb d'intervalles en rayon,
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre 'pos' ou 'neg' de l'ecoulement,
		* ind_eig_b : vecteur propre 'pos' ou 'neg' de la perturbation magnetique,
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle a l'ICB,
		* bc_o_temp : 1 = Flux impose a la CMB ; 2 = T imposee a la CMB.
	Outputs :
		* pol,tor,temp,polb,torb : classes des trois scalaires;
	"""
	#-------------------------------------------------------------------
	# Classes
	class scalar:
		"Definition de la classe scalar pour pol, tor, temp, chi, polb, torb"
	pol = scalar()
	tor = scalar()
	temp = scalar()
	chi = scalar()
	polb = scalar()
	torb = scalar()
	
	# Nb d'elements en rayon selon les BC (sauf pour B)
	pol.nbel = N-1
	tor.nbel = N-1
	temp.nbel = N-1
	chi.nbel = N-1
	
	if bc_i == 1:
		tor.nbel  = tor.nbel  + 1
	if bc_o == 1:
		tor.nbel  = tor.nbel  + 1
	if bc_i_temp == 1:
		temp.nbel = temp.nbel + 1
	if bc_o_temp == 1:
		temp.nbel = temp.nbel + 1
	if bc_i_chi == 1:
		chi.nbel = chi.nbel + 1
	if bc_o_chi == 1:
		chi.nbel = chi.nbel + 1
	
	# Nb d'elements fixe pour b (isolant a la CMB et nul au centre)
	polb.nbel = N
	torb.nbel = N-1
	if bc_i != 0:
		polb.nbel = polb.nbel + 1
	
	# Choix du vecteur propre pos
	if ind_eig == 'pos':
	# P_1, P3,..., P_{L-1} et T_2, T_3,..., T_{L}
		if   Lmax%2 == 0 and m == 0:
		# Harmoniques commencent a P_1 T_2 et terminent a P_{Lmax-1} et T_Lmax
			pol.Lmin  = 1
			tor.Lmin  = 2
			temp.Lmin = 1
			chi.Lmin = 1
			pol.nbl  = Lmax//2
			tor.nbl  = Lmax//2
			temp.nbl = Lmax//2
			chi.nbl = Lmax//2
			pol.nbel_vec  = pol.nbel * pol.nbl
			tor.nbel_vec  = tor.nbel * tor.nbl
			temp.nbel_vec = temp.nbel * temp.nbl
			chi.nbel_vec = chi.nbel * chi.nbl
			pol.L  = np.arange(pol.Lmin,Lmax,2)
			tor.L  = np.arange(tor.Lmin,Lmax+1,2)
			temp.L = np.arange(temp.Lmin,Lmax,2)
			chi.L = np.arange(chi.Lmin,Lmax,2)
		elif Lmax%2 != 0 and m%2 == 0 and m > 0:
		# Harmoniques commencent a P_{m+1} T_m et terminent a P_Lmax et T_{Lmax-1}
			pol.Lmin  = m+1
			tor.Lmin  = m
			temp.Lmin = m+1
			chi.Lmin = m+1
			pol.nbl   = (Lmax - pol.Lmin + 2)//2
			tor.nbl   = pol.nbl
			temp.nbl  = pol.nbl
			chi.nbl  = pol.nbl
			pol.nbel_vec  = pol.nbel  * pol.nbl
			tor.nbel_vec  = tor.nbel  * tor.nbl
			temp.nbel_vec = temp.nbel * pol.nbl
			chi.nbel_vec = chi.nbel * pol.nbl
			pol.L  = np.arange(pol.Lmin,Lmax+1,2)
			tor.L  = np.arange(tor.Lmin,Lmax,2)
			temp.L = np.arange(temp.Lmin,Lmax+1,2)
			chi.L = np.arange(chi.Lmin,Lmax+1,2)
		elif Lmax%2 == 0 and m%2 != 0:
		# Harmoniques commence a P_{m} T_{m+1} et terminent a P_{Lmax-1} et T_Lmax
			pol.Lmin  = m
			tor.Lmin  = m+1
			temp.Lmin = m
			chi.Lmin = m
			pol.nbl   = (Lmax - pol.Lmin + 1)//2
			tor.nbl   = pol.nbl
			temp.nbl  = pol.nbl
			chi.nbl  = pol.nbl
			pol.nbel_vec  = pol.nbel  * pol.nbl
			tor.nbel_vec  = tor.nbel  * tor.nbl
			temp.nbel_vec = temp.nbel * temp.nbl
			chi.nbel_vec = chi.nbel * chi.nbl
			pol.L  = np.arange(pol.Lmin,Lmax,2)
			tor.L  = np.arange(tor.Lmin,Lmax+1,2)
			temp.L = np.arange(temp.Lmin,Lmax,2)
			chi.L = np.arange(chi.Lmin,Lmax,2)

	# Choix du vecteur propre neg
	elif ind_eig == 'neg':
	# P_2, P4, ..., P_{L} et T_1, T_3, ..., T_{L-1}
		if   Lmax%2 == 0 and m == 0:
		# Harmoniques commencent a T_1 P_2 et terminent a T_{Lmax-1} et P_Lmax
			tor.Lmin  = 1
			pol.Lmin  = 2
			temp.Lmin = 2
			chi.Lmin = 2
			pol.nbl  = Lmax//2
			tor.nbl  = Lmax//2
			temp.nbl = Lmax//2
			chi.nbl = Lmax//2
			pol.nbel_vec  = pol.nbel * pol.nbl
			tor.nbel_vec  = tor.nbel * tor.nbl
			temp.nbel_vec = temp.nbel * temp.nbl
			chi.nbel_vec = chi.nbel * chi.nbl
			pol.L  = np.arange(pol.Lmin,Lmax+1,2)
			tor.L  = np.arange(tor.Lmin,Lmax,2)
			temp.L = np.arange(temp.Lmin,Lmax+1,2)
			chi.L = np.arange(chi.Lmin,Lmax+1,2)
		elif Lmax%2 != 0 and m%2 == 0 and m > 0:
		# Harmoniques commencent a T_{m+1} P_m et terminent a T_Lmax et P_{Lmax-1}
			pol.Lmin  = m
			tor.Lmin  = m+1
			temp.Lmin = m
			chi.Lmin = m
			tor.nbl   = (Lmax - tor.Lmin + 2)//2
			pol.nbl   = tor.nbl
			temp.nbl  = pol.nbl
			chi.nbl  = pol.nbl
			pol.nbel_vec  = pol.nbel  * pol.nbl
			tor.nbel_vec  = tor.nbel  * tor.nbl
			temp.nbel_vec = temp.nbel * pol.nbl
			chi.nbel_vec = chi.nbel * pol.nbl
			pol.L  = np.arange(pol.Lmin,Lmax,2)
			tor.L  = np.arange(tor.Lmin,Lmax+1,2)
			temp.L = np.arange(temp.Lmin,Lmax,2)
			chi.L = np.arange(chi.Lmin,Lmax,2)
		elif Lmax%2 == 0 and m%2 != 0:
		# Harmoniques commence a T_{m} P_{m+1} et terminent a T_{Lmax-1} et P_Lmax
			pol.Lmin  = m+1
			tor.Lmin  = m
			temp.Lmin = m+1
			chi.Lmin = m+1
			tor.nbl   = (Lmax - tor.Lmin + 1)//2
			pol.nbl   = tor.nbl
			temp.nbl  = pol.nbl
			chi.nbl  = pol.nbl
			pol.nbel_vec  = pol.nbel  * pol.nbl
			tor.nbel_vec  = tor.nbel  * tor.nbl
			temp.nbel_vec = temp.nbel * temp.nbl
			chi.nbel_vec = chi.nbel * chi.nbl
			pol.L  = np.arange(pol.Lmin,Lmax+1,2)
			tor.L  = np.arange(tor.Lmin,Lmax,2)
			temp.L = np.arange(temp.Lmin,Lmax+1,2)
			chi.L = np.arange(chi.Lmin,Lmax+1,2)
	# Choix du vecteur propre pour b
	if ind_eig_b == 'pos':
	# P_1, P3,..., P_{L-1} et T_2, T_3,..., T_{L}
		if   Lmax%2 == 0 and m == 0:
		# Harmoniques commencent a P_1 T_2 et terminent a P_{Lmax-1} et T_Lmax
			polb.Lmin = 1
			torb.Lmin = 2
			polb.nbl  = Lmax//2
			torb.nbl  = Lmax//2
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax,2)
			torb.L = np.arange(torb.Lmin,Lmax+1,2)
		elif Lmax%2 != 0 and m%2 == 0 and m > 0:
		# Harmoniques commencent a P_{m+1} T_m et terminent a P_Lmax et T_{Lmax-1}
			polb.Lmin = m+1
			torb.Lmin = m
			polb.nbl  = (Lmax - polb.Lmin + 2)//2
			torb.nbl  = polb.nbl
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax+1,2)
			torb.L = np.arange(torb.Lmin,Lmax,2)
		elif Lmax%2 == 0 and m%2 != 0:
		# Harmoniques commence a P_{m} T_{m+1} et terminent a P_{Lmax-1} et T_Lmax
			polb.Lmin = m
			torb.Lmin = m+1
			polb.nbl  = (Lmax - polb.Lmin + 1)//2
			torb.nbl  = polb.nbl
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax,2)
			torb.L = np.arange(torb.Lmin,Lmax+1,2)
	# Choix du vecteur propre neg
	elif ind_eig_b == 'neg':
	# P_2, P4, ..., P_{L} et T_1, T_3, ..., T_{L-1}
		if   Lmax%2 == 0 and m == 0:
		# Harmoniques commencent a T_1 P_2 et terminent a T_{Lmax-1} et P_Lmax
			torb.Lmin = 1
			polb.Lmin = 2
			polb.nbl  = Lmax//2
			torb.nbl  = Lmax//2
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax+1,2)
			torb.L = np.arange(torb.Lmin,Lmax,2)
		elif Lmax%2 != 0 and m%2 == 0 and m > 0:
		# Harmoniques commencent a T_{m+1} P_m et terminent a T_Lmax et P_{Lmax-1}
			polb.Lmin = m
			torb.Lmin = m+1
			torb.nbl  = (Lmax - torb.Lmin + 2)//2
			polb.nbl  = torb.nbl
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax,2)
			torb.L = np.arange(torb.Lmin,Lmax+1,2)
		elif Lmax%2 == 0 and m%2 != 0:
		# Harmoniques commence a T_{m} P_{m+1} et terminent a T_{Lmax-1} et P_Lmax
			polb.Lmin = m+1
			torb.Lmin = m
			torb.nbl  = (Lmax - torb.Lmin + 1)//2
			polb.nbl  = torb.nbl
			polb.nbel_vec = polb.nbel * polb.nbl
			torb.nbel_vec = torb.nbel * torb.nbl
			polb.L = np.arange(polb.Lmin,Lmax+1,2)
			torb.L = np.arange(torb.Lmin,Lmax,2)
#	else:
#		pass
	return pol, tor, temp, chi, polb, torb

def ind_eig_mat(scal_vec, scal_eqn, L_vec, L_eqn, ind_r_eqn, ind_r_vec, flag_temp, pol, tor, temp, chi, polb, torb):
	#-------------------------------------------------------------------
	"""Retourne positions (I,J) dans la matrice A ou B, pour le scalaire (pol, tor).
	Inputs :
		* scal_vec  : 'pol', 'tor', 'polb', 'torb' pour le vecteur propre,
		* scal_eqn  : 'pol', 'tor', 'polb', 'torb' pour l'equation,
		* L_vec     : degre harmonique pour le vecteur propre,
		* L_eqn     : degre harmonique pour l'equation,
		* ind_r_eqn : indice du rayon dans l'equation,
		* ind_r_vec : indice du rayon dans le vecteur propre,
		* flag_temp : enable or not temperature field,
		* pol,tor,temp,polb,torb : outputs of nb_eig_vec.
	Outputs :
		* I         : ligne dans la matrice,
		* J         : colonne dans la matrice.
		"""
	#-------------------------------------------------------------------
	# Disable or not of the thermal part
	if flag_temp == 0:
		TEMP = 0
	else:
		if flag_temp == 1:
			TEMP = temp.nbel_vec
		elif flag_temp == 2:
			TEMP = temp.nbel_vec + chi.nbel_vec
	
	# Choix du vecteur propre
	if scal_vec == 'pol': 
		if scal_eqn == 'pol':		# Eqn poloidale Matrice carree
			ind_L_vec = np.where(pol.L == L_vec)
			ind_L_eqn = ind_L_vec
			ind_L_vec = ind_L_vec[0][0] # Extraction de l'indice
			ind_L_eqn = ind_L_vec
			I = ind_r_eqn * pol.nbl + ind_L_eqn
			J = ind_r_vec * pol.nbl + ind_L_vec
		elif scal_eqn == 'tor':			# Eqn toroidale
			ind_L_vec = np.where(pol.L == L_vec)
			ind_L_eqn = np.where(tor.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * tor.nbl + ind_L_eqn + pol.nbel_vec
			J = ind_r_vec * pol.nbl + ind_L_vec
		elif scal_eqn == 'temp':
			ind_L_vec = np.where(pol.L == L_vec)
			ind_L_eqn = np.where(temp.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * temp.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec
			J = ind_r_vec * pol.nbl  + ind_L_vec
		elif scal_eqn == 'chi':
			ind_L_vec = np.where(pol.L == L_vec)
			ind_L_eqn = np.where(chi.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * chi.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
			J = ind_r_vec * pol.nbl + ind_L_vec
		#~ elif scal_eqn == 'polb':			# Eqn toroidale
			#~ ind_L_vec = np.where(pol.L == L_vec)
			#~ ind_L_eqn = np.where(polb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * polb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP
#~ #			I = ind_r_eqn * polb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec
			#~ J = ind_r_vec * pol.nbl + ind_L_vec
		#~ elif scal_eqn == 'torb':			# Eqn toroidale
			#~ ind_L_vec = np.where(pol.L == L_vec)
			#~ ind_L_eqn = np.where(torb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * torb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
			#~ J = ind_r_vec * pol.nbl + ind_L_vec
	elif scal_vec == 'tor':
		if scal_eqn == 'pol':
			ind_L_vec = np.where(tor.L == L_vec)
			ind_L_eqn = np.where(pol.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * pol.nbl + ind_L_eqn
			J = ind_r_vec * tor.nbl + ind_L_vec + pol.nbel_vec
		elif scal_eqn == 'tor':			# Matrice carree
			ind_L_vec = np.where(tor.L == L_vec)
			ind_L_eqn = ind_L_vec
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_vec
			I = ind_r_eqn * tor.nbl + ind_L_eqn + pol.nbel_vec
			J = ind_r_vec * tor.nbl + ind_L_vec + pol.nbel_vec
		#~ elif scal_eqn == 'temp':
			#~ ind_L_vec = np.where(tor.L  == L_vec)
			#~ ind_L_eqn = np.where(temp.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * temp.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec
			#~ J = ind_r_vec * tor.nbl  + ind_L_vec + pol.nbel_vec
		#~ elif scal_eqn == 'chi':
			#~ ind_L_vec = np.where(tor.L  == L_vec)
			#~ ind_L_eqn = np.where(chi.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * chi.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP
			#~ J = ind_r_vec * tor.nbl  + ind_L_vec + pol.nbel_vec
		#~ elif scal_eqn == 'polb':
			#~ ind_L_vec = np.where(tor.L  == L_vec)
			#~ ind_L_eqn = np.where(polb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * polb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP
			#~ J = ind_r_vec * tor.nbl + ind_L_vec + pol.nbel_vec
		#~ elif scal_eqn == 'torb':
			#~ ind_L_vec = np.where(tor.L  == L_vec)
			#~ ind_L_eqn = np.where(torb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * torb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
			#~ J = ind_r_vec * tor.nbl + ind_L_vec + pol.nbel_vec
	elif scal_vec == 'temp':
		if scal_eqn == 'pol':
			ind_L_vec = np.where(temp.L == L_vec)
			ind_L_eqn = np.where(pol.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * pol.nbl  + ind_L_eqn
			J = ind_r_vec * temp.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec
		#~ elif scal_eqn == 'tor':
			#~ ind_L_vec = np.where(temp.L == L_vec)
			#~ ind_L_eqn = np.where(tor.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * tor.nbl  + ind_L_eqn + pol.nbel_vec
			#~ J = ind_r_vec * temp.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec
		elif scal_eqn == 'temp':		# Matrice carree
			ind_L_vec = np.where(temp.L == L_vec)
			ind_L_eqn = ind_L_vec
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_vec
			I = ind_r_eqn * temp.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec
			J = ind_r_vec * temp.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec
	elif scal_vec == 'chi':
		if scal_eqn == 'pol':
			ind_L_vec = np.where(chi.L == L_vec)
			ind_L_eqn = np.where(pol.L == L_eqn)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_eqn[0][0]
			I = ind_r_eqn * pol.nbl  + ind_L_eqn
			J = ind_r_vec * chi.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
		#~ elif scal_eqn == 'tor':
			#~ ind_L_vec = np.where(chi.L == L_vec)
			#~ ind_L_eqn = np.where(tor.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * tor.nbl  + ind_L_eqn + pol.nbel_vec
			#~ J = ind_r_vec * chi.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec
		elif scal_eqn == 'chi':		# Matrice carree
			ind_L_vec = np.where(chi.L == L_vec)
			ind_L_eqn = ind_L_vec
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			ind_L_eqn = ind_L_vec
			I = ind_r_eqn * chi.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
			J = ind_r_vec * chi.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
	#~ elif scal_vec == 'polb': 
		#~ if scal_eqn == 'pol':
			#~ ind_L_vec = np.where(polb.L == L_vec)
			#~ ind_L_eqn = np.where(pol.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0] # Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * pol.nbl  + ind_L_eqn
			#~ J = ind_r_vec * polb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP
		#~ elif scal_eqn == 'tor':
			#~ ind_L_vec = np.where(polb.L == L_vec)
			#~ ind_L_eqn = np.where(tor.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * tor.nbl + ind_L_eqn + pol.nbel_vec
			#~ J = ind_r_vec * polb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP
		#~ elif scal_eqn == 'polb':
			#~ ind_L_vec = np.where(polb.L == L_vec)
			#~ ind_L_eqn = ind_L_vec
			#~ ind_L_vec = ind_L_vec[0][0] # Extraction de l'indice
			#~ ind_L_eqn = ind_L_vec
			#~ I = ind_r_eqn * polb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP
			#~ J = ind_r_vec * polb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP
		#~ elif scal_eqn == 'torb':
			#~ ind_L_vec = np.where(polb.L == L_vec)
			#~ ind_L_eqn = np.where(torb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * torb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
			#~ J = ind_r_vec * polb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP
	#~ elif scal_vec == 'torb':
		#~ if scal_eqn == 'pol':
			#~ ind_L_vec = np.where(torb.L == L_vec)
			#~ ind_L_eqn = np.where(pol.L  == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * pol.nbl + ind_L_eqn
			#~ J = ind_r_vec * torb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
		#~ elif scal_eqn == 'tor':			# Matrice carree
			#~ ind_L_vec = np.where(torb.L == L_vec)
			#~ ind_L_eqn = np.where(tor.L  == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * tor.nbl  + ind_L_eqn + pol.nbel_vec
			#~ J = ind_r_vec * torb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
		#~ elif scal_eqn == 'polb':
			#~ ind_L_vec = np.where(torb.L == L_vec)
			#~ ind_L_eqn = np.where(polb.L == L_eqn)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn = ind_L_eqn[0][0]
			#~ I = ind_r_eqn * polb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP
			#~ J = ind_r_vec * torb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
		#~ elif scal_eqn == 'torb':			# Matrice carree
			#~ ind_L_vec  = np.where(torb.L == L_vec)
			#~ ind_L_eqn  = ind_L_vec
			#~ ind_L_vec  = ind_L_vec[0][0]	# Extraction de l'indice
			#~ ind_L_eqn  = ind_L_vec
			#~ I = ind_r_eqn * torb.nbl + ind_L_eqn + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
			#~ J = ind_r_vec * torb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
	
	return I,J

def ind_eig_vec(scal_vec, L_vec, ind_r_vec, flag_temp, pol, tor, temp, chi, polb, torb):
	#-------------------------------------------------------------------
	"""Retourne la position J pour le scalaire (pol, tor) dans le vecteur propre.
	Inputs :
		* scal_vec  : 'pol', 'tor', 'polb', 'torb' pour le vecteur propre,
		* L_vec     : degre harmonique pour le vecteur propre,
		* ind_r_vec : indice du rayon dans le vecteur propre,
		* flag_temp : enable or not the temperature field.
		* pol,tor,temp,polb,torb : outputs of nb_eig_vec.
	Output :
		* J         : colonne dans la matrice.
	"""
	#-------------------------------------------------------------------
	# Disable or not of the thermal part
	if flag_temp == 0:
		TEMP = 0
	else:
		if flag_temp == 1:
			TEMP = temp.nbel_vec
		elif flag_temp == 2:
			TEMP = temp.nbel_vec + chi.nbel_vec
	
	# Choix du vecteur propre
	if scal_vec == 'pol': 
			ind_L_vec = np.where(pol.L == L_vec)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			J = ind_r_vec * pol.nbl + ind_L_vec
	elif scal_vec == 'tor':
			ind_L_vec = np.where(tor.L == L_vec)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			J = ind_r_vec * tor.nbl + ind_L_vec + pol.nbel_vec
	elif scal_vec == 'temp':
			ind_L_vec = np.where(temp.L == L_vec)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			J = ind_r_vec * temp.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec
	elif scal_vec == 'chi':
			ind_L_vec = np.where(chi.L == L_vec)
			ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			J = ind_r_vec * chi.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
	#~ elif scal_vec == 'polb':
			#~ ind_L_vec = np.where(polb.L == L_vec)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ J = ind_r_vec * polb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP
	#~ elif scal_vec == 'torb':
			#~ ind_L_vec = np.where(torb.L == L_vec)
			#~ ind_L_vec = ind_L_vec[0][0]	# Extraction de l'indice
			#~ J = ind_r_vec * torb.nbl + ind_L_vec + pol.nbel_vec + tor.nbel_vec + TEMP + polb.nbel_vec
	
	return J

def choice_eig(ind_vec, scal, sol, flag_temp, m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi):
	#-------------------------------------------------------------------
	""" Choix de la valeur propre puis puis mise en forme du vecteur propre.
	Inputs :
		* ind_vec   : indice de la valeur propre,
		* scal      : scalaire [pol, tor],
		* sol       : matrix of eigenvectors in columns,
		* flag_temp : enable or not the temperature field.
		* m         : ordre azimuthal du mode,
		* N         : nb d'intervalles en rayon,
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre 'pos' ou 'neg',
		* ind_eig_b : magnetic eigenvector ['pos' or 'neg'],
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
	Output :
		* vec       : vecteur propre pour le scalaire.
	"""
	#-------------------------------------------------------------------
	# Import des constantes
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	
	# Choix du scalaire
	if scal == 'pol':
		vec   = np.zeros((pol.nbel,pol.nbl),dtype=complex)
		ind_l = 0
		for l in range(pol.Lmin, pol.L[-1]+1,2):
			for k in range(0, pol.nbel):
				j = ind_eig_vec('pol', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				vec[k,ind_l] = sol.vec[j,ind_vec]
#				vec[k,ind_l] = sol[j,ind_vec]
			ind_l = ind_l + 1
	elif scal == 'tor':
		vec = np.zeros((tor.nbel,tor.nbl),dtype=complex)
		ind_l = 0
		for l in range(tor.Lmin, tor.L[-1]+1,2):
			for k in range(0, tor.nbel):
				j = ind_eig_vec('tor', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				vec[k,ind_l] = sol.vec[j,ind_vec]
#				vec[k,ind_l] = sol[j,ind_vec]
			ind_l = ind_l + 1
	elif scal == 'temp':
		vec = np.zeros((temp.nbel,temp.nbl),dtype=complex)
		ind_l = 0
		for l in range(temp.Lmin, temp.L[-1]+1,2):
			for k in range(0,temp.nbel):
				j = ind_eig_vec('temp', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				vec[k,ind_l] = sol.vec[j,ind_vec]
			ind_l = ind_l + 1
	elif scal == 'chi':
		vec = np.zeros((chi.nbel,chi.nbl),dtype=complex)
		ind_l = 0
		for l in range(chi.Lmin, chi.L[-1]+1,2):
			for k in range(0,chi.nbel):
				j = ind_eig_vec('chi', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				vec[k,ind_l] = sol.vec[j,ind_vec]
			ind_l = ind_l + 1
	#~ elif scal == 'polb':
		#~ vec   = np.zeros((polb.nbel, polb.nbl),dtype=complex)
		#~ ind_l = 0
		#~ for l in range(polb.Lmin, polb.L[-1]+1,2):
			#~ for k in range(0,polb.nbel):
				#~ j = ind_eig_vec('polb', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				#~ vec[k,ind_l] = sol.vec[j,ind_vec]
#~ #				vec[k,ind_l] = sol[j,ind_vec]
			#~ ind_l = ind_l + 1
	#~ elif scal == 'torb':
		#~ vec = np.zeros((torb.nbel, torb.nbl),dtype=complex)
		#~ ind_l = 0
		#~ for l in range(torb.Lmin, torb.L[-1]+1,2):
			#~ for k in range(0,torb.nbel):
				#~ j = ind_eig_vec('torb', l, k, flag_temp, pol, tor, temp, chi, polb, torb)
				#~ vec[k,ind_l] = sol.vec[j,ind_vec]
#~ #				vec[k,ind_l] = sol[j,ind_vec]
			#~ ind_l = ind_l + 1
	return vec

def scal_shtns(scal, vec, m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi):
	#-------------------------------------------------------------------
	"""Reshape for shtns
	Inputs :
		* scal      : scalar [pol, tor, temp, chi],
		* vec       : eigenvector given by choice_eig,
		* m         : azimuthal wave number,
		* N         : nb of radial shells,
		* Lmax      : spherical harmonic degree [entier pair],
		* ind_eig   : symetry ['pos' ou 'neg'] of the hydrodynamic solution
		* ind_eig_b : NO LONGER USED
		* bc_i      : 0 = full sphere; 1 = stress-free at ICB ; 2 = no-slip at ICB,
		* bc_o      : 1 = stress-free at CMB; 2 = no-slip a la CMB,
		* bc_i_temp : 0 = full sphere; 1 = fixed flux at ICB ; 2 = fixed temperature at ICB,
		* bc_o_temp : 1 = fixed flux at CMB ; 2 = fixed compoisition at CMB.
	Output :
		* vec : eigenvector in shtns format.
	"""
	#-------------------------------------------------------------------
	# Import des constantes
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	
	# Choix du scalaire
	if scal == 'pol':
		if m == 0:
			vec_shtns  = np.zeros((pol.nbel,2*pol.nbl+1),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(pol.L[0] == L)[0][0]
			j_end = np.where(pol.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
		elif m > 0:
			vec_shtns  = np.zeros((pol.nbel,2*pol.nbl),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(pol.L[0] == L)[0][0]
			j_end = np.where(pol.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
			# Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			vec_shtns = np.hstack([np.zeros((pol.nbel,Lmax+1),dtype=complex), vec_shtns])
	elif scal == 'tor':
		if m == 0:
			vec_shtns  = np.zeros((tor.nbel,2*tor.nbl+1),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(tor.L[0] == L)[0][0]
			j_end = np.where(tor.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
		elif m > 0:
			vec_shtns  = np.zeros((tor.nbel,2*tor.nbl),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(tor.L[0] == L)[0][0]
			j_end = np.where(tor.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
			# Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			vec_shtns = np.hstack([np.zeros((tor.nbel,Lmax+1),dtype=complex), vec_shtns])
	elif scal =='temp':
		if m == 0:
			vec_shtns  = np.zeros((temp.nbel,2*temp.nbl+1),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(temp.L[0] == L)[0][0]
			j_end = np.where(temp.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
		elif m > 0:
			vec_shtns  = np.zeros((temp.nbel,2*temp.nbl),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(temp.L[0] == L)[0][0]
			j_end = np.where(temp.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
			# Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			vec_shtns = np.hstack([np.zeros((temp.nbel,Lmax+1),dtype=complex), vec_shtns])
	elif scal =='chi':
		if m == 0:
			vec_shtns  = np.zeros((chi.nbel,2*chi.nbl+1),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(chi.L[0] == L)[0][0]
			j_end = np.where(chi.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
		elif m > 0:
			vec_shtns  = np.zeros((chi.nbel,2*chi.nbl),dtype=complex)
			L = np.arange(m,Lmax+1)
			j_ini = np.where(chi.L[0] == L)[0][0]
			j_end = np.where(chi.L[-1] == L)[0][0]
			k = 0
			for j in range(j_ini,j_end+1,2):
				vec_shtns[0:,j] = vec[0:,k]
				k = k+1
			# Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			vec_shtns = np.hstack([np.zeros((chi.nbel,Lmax+1),dtype=complex), vec_shtns])
	#~ if scal == 'polb':
		#~ if   m == 0:
			#~ vec_shtns  = np.zeros((polb.nbel,2*polb.nbl+1),dtype=complex)
			#~ L = np.arange(m,Lmax+1)
			#~ j_ini = np.where(polb.L[0] == L)[0][0]
			#~ j_end = np.where(polb.L[-1] == L)[0][0]
			#~ k = 0
			#~ for j in range(j_ini,j_end+1,2):
				#~ vec_shtns[0:,j] = vec[0:,k]
				#~ k = k+1
		#~ elif m > 0:
			#~ vec_shtns  = np.zeros((polb.nbel,2*polb.nbl),dtype=complex)
			#~ L = np.arange(m,Lmax+1)
			#~ j_ini = np.where(polb.L[0] == L)[0][0]
			#~ j_end = np.where(polb.L[-1] == L)[0][0]
			#~ k = 0
			#~ for j in range(j_ini,j_end+1,2):
				#~ vec_shtns[0:,j] = vec[0:,k]
				#~ k = k+1
			#~ # Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			#~ vec_shtns = np.hstack([np.zeros((polb.nbel,Lmax+1),dtype=complex), vec_shtns])
	#~ elif scal == 'torb':
		#~ if   m == 0:
			#~ vec_shtns  = np.zeros((torb.nbel,2*torb.nbl+1),dtype=complex)
			#~ L = np.arange(m,Lmax+1)
			#~ j_ini = np.where(torb.L[0] == L)[0][0]
			#~ j_end = np.where(torb.L[-1] == L)[0][0]
			#~ k = 0
			#~ for j in range(j_ini,j_end+1,2):
				#~ vec_shtns[0:,j] = vec[0:,k]
				#~ k = k+1
		#~ elif m > 0:
			#~ vec_shtns  = np.zeros((torb.nbel,2*torb.nbl),dtype=complex)
			#~ L = np.arange(m,Lmax+1)
			#~ j_ini = np.where(torb.L[0] == L)[0][0]
			#~ j_end = np.where(torb.L[-1] == L)[0][0]
			#~ k = 0
			#~ for j in range(j_ini,j_end+1,2):
				#~ vec_shtns[0:,j] = vec[0:,k]
				#~ k = k+1
			#~ # Concatenation des coefs nuls pour l = 0:Lmax et m = 0 (dans tous les cas)
			#~ vec_shtns = np.hstack([np.zeros((torb.nbel,Lmax+1),dtype=complex), vec_shtns])
	return vec_shtns

def coeff_stdY_ctY(scal_vec, scal_eqn, ind_eig, ind_eig_b, sh, m, Lmax, pol, tor):
	#-------------------------------------------------------------------
	"""
	Compute the spherical harmonic coefficients of the projections cos(theta) Ylm and sin(theta) * dYlm/dtheta onto Y_{l-1}^m and Y_{l+1}^m.
	Inputs:
		* scal_vec  : scalar of the (eigen)vector ['pol', 'tor', 'polb', 'torb'],
		* scal_eqn  : scalar of the equation ['pol', 'tor', 'polb', 'torb'],
		* ind_eig   : equatorial symmetry of the hydrodynamic (eigen)vector ['pos' ou 'neg'],
		* ind_eig_b : equatorial symmetry of the magnetic (eigen)vector ['pos' ou 'neg'],
		* sh        : python instance of SHTNS,
		* m         : azimuthal order,
		* Lmax      : truncation of the spherical harmonic degrees,
		* pol       : python instance of the poloidal-like scalar [only spherical degrees matter], in pratice the poloidal one,
		* tor       : python instance of the toroidal-like scalar [only spherical degrees matter], in pratice the toroidal one.
	Outputs:
		* l_p1 : spherical harmonic degrees for the loop on the scalar equation for Y_{l+1}^m,
		* l_m1 : spherical harmonic degrees for the loop on the scalar equation for Y_{l-1}^m,
		* coeff_p1_stdY : spherical harmonic coefficients of sin(theta) * dYlm/dtheta of Y_{l+1}^m for each l in l_p1,
		* coeff_m1_stdY : spherical harmonic coefficients of sin(theta) * dYlm/dtheta of Y_{l-1}^m for each l in l_m1,
		* coeff_p1_ctY  : spherical harmonic coefficients of cos(theta) * Ylm of Y_{l+1}^m for each l in l_p1,
		* coeff_m1_ctY  : spherical harmonic coefficients of cos(theta) * Ylm of Y_{l-1}^m  for each l in l_m1,
	"""
	#--------------------------------------------------------------------
	# Initialization
	coeff_p1_stdY = []
	coeff_m1_stdY = []
	coeff_p1_ctY  = []
	coeff_m1_ctY  = []
	ct_Y  = sh.mul_ct_matrix()
	st_dY = sh.st_dt_matrix()
	
	# Check the equatorial symetric behaviour
	if ind_eig == ind_eig_b:
		Scal1 = ['pol','polb']
		Scal2 = ['tor', 'torb']
	if ind_eig != ind_eig_b:
		Scal1 = ['pol','torb']
		Scal2 = ['tor', 'polb']
	
	#-------------------------------------------------------------------
	# Coupling coefficients
	# Loop of spherical harmonic degree l of the equation
	#--------------------------------------------------------------------
	if scal_eqn in Scal1:	# 'Poloidal'-like equation
		if scal_vec in Scal2:
			if ind_eig == 'pos':
				if Lmax%2 == 0: # m odd et 0 
					# Y_{l+1}
					l_p1 = np.arange(pol.L[1],pol.L[-1]+1,2,dtype=int)	# L[1] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(pol.Lmin,pol.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
				elif Lmax%2 != 0:	# m even
					# Y_{l+1}
					l_p1 = np.arange(pol.Lmin,pol.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(pol.Lmin,pol.L[-1],2,dtype=int)	# L[0] a L[-2] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
			elif ind_eig == 'neg':
				if Lmax%2 == 0: # m impair et 0 
					# Y_{l+1}
					l_p1 = np.arange(pol.Lmin,pol.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(pol.Lmin,pol.L[-1],2,dtype=int)	# L[0] a L[-2] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
				elif Lmax%2 != 0:
					# Y_{l+1}
					l_p1 = np.arange(pol.L[1],pol.L[-1]+1,2,dtype=int)	# L[1] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(pol.Lmin,pol.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
			return l_p1, l_m1, np.array(coeff_p1_stdY), np.array(coeff_m1_stdY), np.array(coeff_p1_ctY), np.array(coeff_m1_ctY)
		else:
			print('Not a coupling term cos(theta) Ylm nor sin(theta) dYlm/dtheta...')
			return
	elif scal_eqn in Scal2:	# 'Toroidal'-like equation
		if scal_vec in Scal1:
			if ind_eig == 'pos':
				if Lmax%2 == 0: # m odd and 0 
					# Y_{l+1}
					l_p1 = np.arange(tor.Lmin,tor.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(tor.Lmin,tor.L[-1],2,dtype=int)	# L[0] a L[-2] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
				elif Lmax%2 != 0:
					# Y_{l+1}
					l_p1 = np.arange(tor.L[1],tor.L[-1]+1,2,dtype=int)	# L[1] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(tor.Lmin,tor.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
			elif ind_eig == 'neg':
				if Lmax%2 == 0: # m impair et 0 
					# Y_{l+1}
					l_p1 = np.arange(tor.L[1],tor.L[-1]+1,2,dtype=int)	# L[1] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(tor.Lmin,tor.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
				elif Lmax%2 != 0:
					# Y_{l+1}
					l_p1 = np.arange(tor.Lmin,tor.L[-1]+1,2,dtype=int)	# L[0] a L[-1] inclu
					for l in l_p1:
						ind_Ylm_pos = 2*sh.idx(int(l-1),int(m))+1
						coeff_p1_stdY.append(st_dY[ind_Ylm_pos+1])
						coeff_p1_ctY.append(ct_Y[ind_Ylm_pos+1])
					# Y_{l-1}
					l_m1 = np.arange(tor.Lmin,tor.L[-1],2,dtype=int)	# L[0] a L[-2] inclu
					for l in l_m1:
						ind_Ylm_neg = 2*sh.idx(int(l+1),int(m))
						coeff_m1_stdY.append(st_dY[ind_Ylm_neg-1])
						coeff_m1_ctY.append(ct_Y[ind_Ylm_neg-1])
			return l_p1, l_m1, np.array(coeff_p1_stdY), np.array(coeff_m1_stdY), np.array(coeff_p1_ctY), np.array(coeff_m1_ctY)
		else:
			print('Not a coupling term cos(theta) Ylm nor sin(theta) dYlm/dtheta...')
			return
	else:
		print('The scalar does not exist !')
		return

#-----------------------------------------------------------------------
# Filling of the matrix A
#-----------------------------------------------------------------------
def hydro_eig_A(A, error, grid, sh, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, Omega0, nu, flag_temp):
	#-----------------------------------------------------------------------
	"""Remplissage des blocs inertiels de la matrice A du probleme aux valeurs propres.
	Inputs :
		* A         : Scipy sparse matrix,
		* error     : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid      : classe de la grille radiale,
		* sh        : classe definie avec shtns,
		* m         : ordre azimuthal [entier],
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre ['pos' ou 'neg'],
		* ind_eig_b : magnetic eigenvector ['pos' or 'neg'],
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle,
		* bc_o_temp : 1 = Flux impose nul a la CMB ; 2 = T nulle,
		* Omega0    : angular velocity in the rotating frame,
		* nu        : kinematic viscosity,
		* flag_temp : enable or not the temperature field.
	Output :
		* A         : Scipy sparse matrix.
	"""
	#-------------------------------------------------------------------
	#------------------------
	# Initialisation
	#------------------------
	N     = grid.N
	ct_Y  = sh.mul_ct_matrix()
	st_dY = sh.st_dt_matrix()
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	lapla = sub.mat_lapla(error,grid)
	d1    = sub.mat_deriv1(error,grid)
	
	#------------------------
	# Bloc poloidal-poloidal : Laplacien et Bilaplacien
	#------------------------
	for l in range(pol.Lmin,pol.L[-1]+1,2):
		# Non-zero entries
		M_delta = lapla - l*(l+1) * np.diag(1/(grid.r[0,1:-1]**2))
		M_delta = 1j * 2*Omega0 * m/(l*(l+1)) * M_delta
		if nu != 0:
			M_delta2 = nu * sub.mat_bilapla_pol(lapla,grid,l,bc_i,bc_o)
			M_delta = M_delta + M_delta2
			del M_delta2
		i_delta, j_delta = np.nonzero(M_delta)
		I,J = ind_eig_mat('pol', 'pol', l, l, i_delta , j_delta, flag_temp, pol, tor, temp, chi, polb, torb)
		# Assignment
		for k in range(len(I)):
			A[I[k],J[k]] = M_delta[i_delta[k],j_delta[k]]
		del M_delta,i_delta,j_delta,I,J
	
	#------------------------
	# Bloc toroidal - toroidal
	#------------------------
	for l in range(tor.Lmin,tor.L[-1]+1,2):
		# Initialisation
		Id = 1j * 2*Omega0 * m/(l*(l+1)) * np.eye(tor.nbel,tor.nbel,dtype=float)
		if nu != 0:
			M_delta = nu * sub.mat_lapla_tor(lapla,grid,l,bc_i,bc_o)
			Id = Id + M_delta
			del M_delta
		i_tor, j_tor = np.nonzero(Id)
		I,J = ind_eig_mat('tor', 'tor', l, l, i_tor , j_tor, flag_temp, pol, tor, temp, chi, polb, torb)
		# Assignment
		for k in range(len(I)):
			A[I[k],J[k]] = Id[i_tor[k],j_tor[k]]
	del Id,i_tor,j_tor,I,J
	
	#------------------------
	# Bloc toroidal (scal) - poloidal (eqn)
	# Attention au couplage L_1 et L_{-1} qui depend de pos/ned et m/Lmax
	# N-1 eqn pour N-1 a N+1 colonnes
	# Boucles sur les equations pour simplifier les coefficients
	#------------------------
	l_p1,l_m1,coeff_p1_stdY,coeff_m1_stdY,coeff_p1_ctY,coeff_m1_ctY = coeff_stdY_ctY('tor', 'pol', ind_eig, ind_eig_b, sh, m, Lmax, pol, tor)
	# L_1 operator
	for ind in range(len(l_p1)):
		# Non-zero entries
		l = l_p1[ind]
		M_Lpos = 2*Omega0/(l*(l+1)) * sub.mat_L_tor(d1, grid, l-1, 1, coeff_p1_stdY[ind], coeff_p1_ctY[ind], bc_i, bc_o)
		i_MLpos, j_MLpos = np.nonzero(M_Lpos)
		I,J = ind_eig_mat('tor', 'pol', l-1, l, i_MLpos , j_MLpos, flag_temp, pol, tor, temp, chi, polb, torb)
		# Assignment
		for k in range(len(I)):
			A[I[k],J[k]] = M_Lpos[i_MLpos[k],j_MLpos[k]]
		del M_Lpos,i_MLpos,j_MLpos,I,J
	# L_{-1} operator
	for ind in range(len(l_m1)):
		# Non-zero entries
		l = l_m1[ind]
		M_Lneg = 2*Omega0/(l*(l+1)) * sub.mat_L_tor(d1, grid, l+1, -1, coeff_m1_stdY[ind], coeff_m1_ctY[ind], bc_i, bc_o)
		i_MLneg, j_MLneg = np.nonzero(M_Lneg)
		I,J = ind_eig_mat('tor', 'pol', l+1, l, i_MLneg , j_MLneg, flag_temp, pol, tor, temp, chi, polb, torb)
		# Assignment
		for k in range(len(I)):
			A[I[k],J[k]] = M_Lneg[i_MLneg[k],j_MLneg[k]]
		del M_Lneg,i_MLneg,j_MLneg,I,J
	
	#------------------------
	# Bloc poloidal (scal) - toroidal (eqn)
	# Attention au couplage L_1 et L_{-1} qui depend de pos/ned et m/Lmax
	# N-1 a N+1 eqn pour N-1 colonnes
	# Boucles sur les equations pour simplifier les coefficients
	#------------------------
	l_p1,l_m1,coeff_p1_stdY,coeff_m1_stdY,coeff_p1_ctY,coeff_m1_ctY = coeff_stdY_ctY('pol', 'tor', ind_eig, ind_eig_b, sh, m, Lmax, pol, tor)
	
	# L_1 operator
	for ind in range(len(l_p1)):
		# Non-zero entries
		l = l_p1[ind]
		M_Lpos = -2*Omega0/(l*(l+1)) * sub.mat_L_pol(d1, grid, l-1, 1, coeff_p1_stdY[ind], coeff_p1_ctY[ind], bc_i, bc_o)
		i_MLpos, j_MLpos = np.nonzero(M_Lpos)
		I,J = ind_eig_mat('pol', 'tor', l-1, l, i_MLpos , j_MLpos, flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
				A[I[k],J[k]] = M_Lpos[i_MLpos[k],j_MLpos[k]]
	del M_Lpos,i_MLpos,j_MLpos,I,J
	
	# L_{-1} operator
	for ind in range(len(l_m1)):
		# Non-zero entries
		l = l_m1[ind]
		M_Lneg = -2*Omega0/(l*(l+1)) * sub.mat_L_pol(d1, grid, l+1, -1, coeff_m1_stdY[ind], coeff_m1_ctY[ind], bc_i, bc_o)
		i_MLneg, j_MLneg = np.nonzero(M_Lneg)
		I,J = ind_eig_mat('pol', 'tor', l+1, l, i_MLneg , j_MLneg, flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
				A[I[k],J[k]] = M_Lneg[i_MLneg[k],j_MLneg[k]]
	del M_Lneg,i_MLneg,j_MLneg,I,J
	
	return A

def temp_eig_A(A, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, kappa, kappac, Ra, Rac, N2, N2c, gravity, flag_temp):
	#-------------------------------------------------------------------
	"""Remplissage des blocs temperature de la matrice A du probleme aux valeurs propres.
	Inputs :
		* A         : matrice creuse [PETSc],
		* error     : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid      : classe de la grille radiale,
		* m         : ordre azimuthal [entier],
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre []'pos' ou 'neg'],
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle,
		* bc_o_temp : 1 = Flux impose nul a la CMB ; 2 = T nulle,
		* kappa     : thermal diffusivity,
		* Ra        : Rayleigh number,
		* N2        : stratified profile [N+1 points],
		* flag_temp : enable or not the temperature field,
	Output : 
		* A         : scipy sparse matrix.
	"""
	#-------------------------------------------------------------------
	# Init
	#------------------------
	N = grid.N
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	lapla = sub.mat_lapla(error,grid)
	
	#------------------------
	# Bloc temperature - temperature
	#------------------------
	if kappa != 0:
		for l in range(temp.Lmin, temp.L[-1]+1,2):
			# Initialisation
			M_delta = kappa * sub.mat_lapla_temp(lapla,grid,l,bc_i_temp,bc_o_temp)
			i_delta,j_delta = np.nonzero(M_delta)
			I,J = ind_eig_mat('temp', 'temp', l, l, i_delta , j_delta, flag_temp, pol, tor, temp, chi, polb, torb)
			for k in range(len(I)):
				A[I[k],J[k]] = M_delta[i_delta[k],j_delta[k]]
		del M_delta,i_delta,j_delta,I,J
	else:
		print("\t\tBeware: Thermal diffusion disabled!")
		
	#------------------------
	# Bloc poloidal (eqn) - temperature (scal) a redimensionner car temp.nbel >= pol.nbel
	# L_vec = L_eqn ici...
	# N-1 eqn pour (N-1) a (N+1) colonnes
	#------------------------
	if Ra != 0:
		Id = - Ra * np.eye(grid.N+1,dtype=complex) * gravity[0,:]/grid.r[0,:]
			# BC
		Id = np.delete(Id,0,axis=0)	# N*(N+1)
		Id = np.delete(Id,-1,axis=0)	# (N-1)*(N+1)
		if bc_i_temp == 0:
			Id = np.delete(Id,0,axis=1)	# (N-1)*N
		elif bc_i_temp == 2:
			Id = np.delete(Id,0,axis=1)	# (N-1)*N
		if   bc_o_temp == 2:
			Id = np.delete(Id,-1,axis=1)	# (N-1)* N ou N-1
			# Assignment
		i_id,j_id = np.nonzero(Id)
		for l in range(temp.Lmin,temp.L[-1]+1,2):
			I,J = ind_eig_mat('temp', 'pol', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
			for k in range(len(I)):
				A[I[k],J[k]] = Id[i_id[k],j_id[k]]
		del l,i_id,j_id,Id
	
	#------------------------
	# Bloc temperature (eqn) - poloidal (scal) a redimensionner car temp.nbel !>= pol.nbel
	# L_vec = L_eqn ici...
	# (N-1) a (N+1) eqn pour N-1 colonnes
	#------------------------
	if np.any(N2!=0):
		for l in range(pol.Lmin,temp.L[-1]+1,2):
			Id = np.diag(-l*(l+1)*N2[0,1:-1]/grid.r[0,1:-1])	# (N-1)*(N-1)
				# BC
			if bc_i_temp == 1:
				Id = np.vstack([np.zeros((1,N-1)),Id])
			if bc_o_temp == 1:
				Id = np.vstack([Id,np.zeros((1,N-1))])
			i_id,j_id = np.nonzero(Id)
			I,J = ind_eig_mat('pol', 'temp', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
			for k in range(len(I)):
				A[I[k],J[k]] = Id[i_id[k],j_id[k]]
		del Id,i_id,j_id,I,J
	else:
		print("\t\tBeware: Thermal stratification disabled!")
	
	#------------------------
	# Compositional variables
	#------------------------
	if flag_temp == 2:
		#------------------------
		# Bloc chi - chi
		#------------------------
		if kappac != 0:
			for l in range(chi.Lmin, chi.L[-1]+1,2):
				# Initialisation
				M_delta = kappac * sub.mat_lapla_temp(lapla, grid, l, bc_i_chi, bc_o_chi)
				i_delta,j_delta = np.nonzero(M_delta)
				I,J = ind_eig_mat('chi', 'chi', l, l, i_delta , j_delta, flag_temp, pol, tor, temp, chi, polb, torb)
				for k in range(len(I)):
					A[I[k],J[k]] = M_delta[i_delta[k],j_delta[k]]
			del M_delta, i_delta, j_delta, I, J
		else:
			print("\t\tBeware: Compositional diffusion disabled!")
		
		#------------------------
		# Bloc poloidal (eqn) - chi (scal) a redimensionner car chi.nbel >= pol.nbel
		# L_vec = L_eqn ici...
		# N-1 eqn pour (N-1) a (N+1) colonnes
		#------------------------
		if Rac != 0:
			Id = - Rac * np.eye(grid.N+1,dtype=complex) * gravity[0,:]/grid.r[0,:]
				# BC
			Id = np.delete(Id,0,axis=0)	# N*(N+1)
			Id = np.delete(Id,-1,axis=0)	# (N-1)*(N+1)
			if bc_i_chi == 0:
				Id = np.delete(Id,0,axis=1)	# (N-1)*N
			elif bc_i_chi == 2:
				Id = np.delete(Id,0,axis=1)	# (N-1)*N
			if   bc_o_chi == 2:
				Id = np.delete(Id,-1,axis=1)	# (N-1)* N ou N-1
				# Assignment
			i_id,j_id = np.nonzero(Id)
			for l in range(chi.Lmin, chi.L[-1]+1,2):
				I,J = ind_eig_mat('chi', 'pol', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
				for k in range(len(I)):
					A[I[k],J[k]] = Id[i_id[k],j_id[k]]
			del l,i_id,j_id,Id
		
		#------------------------
		# Bloc chi (eqn) - poloidal (scal) a redimensionner car chi.nbel !>= pol.nbel
		# L_vec = L_eqn ici...
		# (N-1) a (N+1) eqn pour N-1 colonnes
		#------------------------
		if np.any(N2c!=0):
			for l in range(pol.Lmin, chi.L[-1]+1,2):
				Id = np.diag(-l*(l+1)*N2c[0,1:-1]/grid.r[0,1:-1])	# (N-1)*(N-1)
					# BC
				if bc_i_chi == 1:
					Id = np.vstack([np.zeros((1,N-1)),Id])
				if bc_o_chi == 1:
					Id = np.vstack([Id,np.zeros((1,N-1))])
				i_id,j_id = np.nonzero(Id)
				I,J = ind_eig_mat('pol', 'chi', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
				for k in range(len(I)):
					A[I[k],J[k]] = Id[i_id[k],j_id[k]]
			del Id,i_id,j_id,I,J
		else:
			print("\t\tBeware: Chemical stratification disabled!")
	
	return A

def buoyancy_eig_A(A, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, Ra, Rac, gravity, flag_temp):
	#-------------------------------------------------------------------
	"""Remplissage des blocs temperature de la matrice A du probleme aux valeurs propres.
	Inputs :
		* A         : matrice creuse [PETSc],
		* error     : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid      : classe de la grille radiale,
		* m         : ordre azimuthal [entier],
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre []'pos' ou 'neg'],
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle,
		* bc_o_temp : 1 = Flux impose nul a la CMB ; 2 = T nulle,
		* bc_i_chi  : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = chi nul,
		* bc_o_chi  : 1 = Flux impose nul a la CMB ; 2 = chi nul,
		* Ra        : Rayleigh number,
		* N2        : stratified profile [N+1 points],
		* flag_temp : enable or not the temperature field,
	Output : 
		* A         : scipy sparse matrix.
	"""
	#-------------------------------------------------------------------
	# Init
	#------------------------
	N = grid.N
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	lapla = sub.mat_lapla(error,grid)
	
	#------------------------
	# Bloc poloidal (eqn) - temperature (scal) a redimensionner car temp.nbel >= pol.nbel
	# L_vec = L_eqn ici...
	# N-1 eqn pour (N-1) a (N+1) colonnes
	#------------------------
	np.seterr(divide='ignore', invalid='ignore')	# avoid warnings from division by zero in the following:
	Id = - Ra * np.eye(grid.N+1,dtype=complex) * gravity[0,:]/grid.r[0,:]
		# BC
	Id = np.delete(Id,0,axis=0)	# N*(N+1)
	Id = np.delete(Id,-1,axis=0)	# (N-1)*(N+1)
	if   bc_i_temp == 0:
		Id = np.delete(Id,0,axis=1)	# (N-1)*N
	elif bc_i_temp == 2:
		Id = np.delete(Id,0,axis=1)	# (N-1)*N
	if   bc_o_temp == 2:
		Id = np.delete(Id,-1,axis=1)	# (N-1)* N ou N-1
		# Assignment
	i_id,j_id = np.nonzero(Id)
	for l in range(temp.Lmin,temp.L[-1]+1,2):
		I,J = ind_eig_mat('temp', 'pol', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
			A[I[k],J[k]] = Id[i_id[k],j_id[k]]
	del l,i_id,j_id,Id
	
	#------------------------
	# Bloc poloidal (eqn) - chi (scal) a redimensionner car chi.nbel >= pol.nbel
	# L_vec = L_eqn ici...
	# N-1 eqn pour (N-1) a (N+1) colonnes
	#------------------------
	if flag_temp == 2:
		Id = - Rac * np.eye(grid.N+1,dtype=complex) * gravity[0,:]/grid.r[0,:]
			# BC
		Id = np.delete(Id,0,axis=0)		# N*(N+1)
		Id = np.delete(Id,-1,axis=0)	# (N-1)*(N+1)
		if bc_i_chi == 0:
			Id = np.delete(Id,0,axis=1)	# (N-1)*N
		elif bc_i_chi == 2:
			Id = np.delete(Id,0,axis=1)	# (N-1)*N
		if bc_o_chi == 2:
			Id = np.delete(Id,-1,axis=1)	# (N-1)* N ou N-1
			# Assignment
		i_id,j_id = np.nonzero(Id)
		for l in range(temp.Lmin,temp.L[-1]+1,2):
			I,J = ind_eig_mat('chi', 'pol', l, l, i_id , j_id, flag_temp, pol, tor, temp, chi, polb, torb)
			for k in range(len(I)):
				A[I[k],J[k]] = Id[i_id[k],j_id[k]]
		del l,i_id,j_id,Id
	
	return A

#-----------------------------------------------------------------------
# Filling of B
#-----------------------------------------------------------------------
def hydro_eig_B(B, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, flag_temp):
	#-------------------------------------------------------------------
	"""Remplissage de la matrice B du probleme aux valeurs propres.
	Inputs :
		* B         : Scipy sparse matrix,
		* error     : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid      : classe de la grille radiale,
		* m         : ordre azimuthal [entier],
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre ['pos' ou 'neg'],
		* ind_eig_b : magnetic eigenvector ['pos' or 'neg'],
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle,
		* bc_o_temp : 1 = Flux impose nul a la CMB ; 2 = T nulle,
		* bc_i_chi  : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = chi nulle,
		* bc_o_chi  : 1 = Flux impose nul a la CMB ; 2 = chi nulle,
		* flag_temp : enable or not the temperature field.
	Output : 
		* B         : scipy sparse matrix.
	"""
	#-------------------------------------------------------------------
	#------------------------
	# Initialisation
	N = grid.N
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	lapla = sub.mat_lapla(error,grid)
	
	#------------------------
	# Bloc poloidal-poloidal 
	#------------------------
	for l in range(pol.Lmin, pol.L[-1]+1,2):
		# Initialisation
		M_delta = lapla - l*(l+1) * np.diag(1/(grid.r[0,1:-1]**2))
		i_delta, j_delta = np.nonzero(M_delta)
		I,J = ind_eig_mat('pol', 'pol', l, l, i_delta , j_delta, flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
			B[I[k],J[k]] = M_delta[i_delta[k],j_delta[k]]
	del M_delta,i_delta,j_delta,I,J
	
	#------------------------
	# Bloc toroidal - toroidal
	#------------------------
	Id = np.eye(tor.nbel, tor.nbel, dtype=complex)
	i_tor, j_tor = np.nonzero(Id)
	for l in range(tor.Lmin, tor.L[-1]+1,2):
		I,J = ind_eig_mat('tor', 'tor', l, l, i_tor, j_tor, flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
			B[I[k],J[k]] = Id[i_tor[k],j_tor[k]]
	del i_tor,j_tor,Id,I,J
	
	return B

def temp_eig_B(B, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, flag_temp):
	#-------------------------------------------------------------------
	"""Remplissage du bloc temperature de la matrice B du probleme aux valeurs propres.
	Inputs :
		* B         : PETSc sparse matrix,
		* error     : erreur dans le coeur du developpement de Taylor [entier pair],
		* grid      : classe de la grille radiale,
		* m         : ordre azimuthal [entier],
		* Lmax      : degre harmonique de troncature [entier pair],
		* ind_eig   : vecteur propre 'pos' ou 'neg' de l'ecoulement,
		* ind_eig_b : vecteur propre 'pos' ou 'neg' de la perturbation magnetique,
		* bc_i      : 0 = BC en r=0 ; 1 = FS a l'ICB ; 2 = NS a l'ICB,
		* bc_o      : 1 = FS a la CMB ; 2 = NS a la CMB,
		* bc_i_temp : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = T nulle,
		* bc_o_temp : 1 = Flux impose nul a la CMB ; 2 = T nulle,
		* bc_i_chi  : 0 = BC en r=0 ; 1 = Flux impose nul a l'ICB ; 2 = chi nul,
		* bc_o_chi  : 1 = Flux impose nul a la CMB ; 2 = chi nul,
		* flag_temp : enable or not the temperature field.
	Output : 
		* B         : scipy sparse matrix.
	"""
	#-------------------------------------------------------------------
	#------------------------
	# Initialisation
	N = grid.N
	pol, tor, temp, chi, polb, torb = nb_eig_vec(m, N, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi)
	
	#------------------------
	# Bloc temperature - temperature
	#------------------------
	Id = np.eye(temp.nbel, temp.nbel,dtype=float)
	i_id, j_id = np.nonzero(Id)
	for l in range(temp.Lmin,temp.L[-1]+1,2):
		I,J = ind_eig_mat('temp', 'temp', l, l, i_id , j_id,  flag_temp, pol, tor, temp, chi, polb, torb)
		for k in range(len(I)):
			B[I[k],J[k]] = Id[i_id[k],j_id[k]]
	del I,J,i_id,j_id,Id
	
	#------------------------
	# Bloc chi - chi
	#------------------------
	if flag_temp == 2:
		Id = np.eye(chi.nbel, chi.nbel,dtype=float)
		i_id, j_id = np.nonzero(Id)
		for l in range(chi.Lmin, chi.L[-1]+1,2):
			I,J = ind_eig_mat('chi', 'chi', l, l, i_id , j_id,  flag_temp, pol, tor, temp, chi, polb, torb)
			for k in range(len(I)):
				B[I[k],J[k]] = Id[i_id[k],j_id[k]]
		del I,J,i_id,j_id,Id
	
	return B
