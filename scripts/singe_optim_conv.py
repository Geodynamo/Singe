# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
# Import of packages
#-----------------------------------------------------------------------
# Compatibility between Python 2.7 and Python 3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# General packages
import os,sys,shutil
sys.path.append(os.getcwd())
import timeit

# Scientific packages
import numpy as np
from math import pi
import scipy.sparse as sc_sparse
import scipy.io as IO
from scipy.optimize import newton, brentq
from mpi4py import MPI

# Hand-made packages
import sub_op_irreg as sub_op
import sub_eig_irreg as sub_eig
import sub_init_spat as init
import params

print("""\n####################################################
\tWelcome in SINGE: LINEAR ONSET
####################################################
""")

#-----------------------------------------------------------------------
# Init
#-----------------------------------------------------------------------
# Path
CWD = os.getcwd()
abs_path_singe = os.path.dirname(os.path.realpath(__file__))
path_mpi = params.path_mpi

# MPI PETSc
comm = MPI.COMM_WORLD
mpi_rank = comm.Get_rank()
mpi_size = comm.Get_size()

# Optimisation
ra_cache = {}	# cache for optimize_Ra
dlogRa = 1		# inital step to find sign change in growth rate
tol = 1e-6
maxiter = 50
signRa = 1		# assume optimizing positive Rayleigh

#-----------------------------------------------------------------------
# function to optimize
#-----------------------------------------------------------------------
### returns the growth rate for given (Ra,Rac)
def optimize_Ra(Ra, A_withoutRa, nb_l, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, Ra2, gr, flag_temp):
	if (Ra,Ra2) in ra_cache:
		#print('\t\tx Ra1 = %g, Ra2 = %g : cache hit!' % (signRa * 10**Ra,Ra2))
		return ra_cache[(Ra,Ra2)]		# don't need to recompute the same value !!
	else:
		# Buoyancy force
		A = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)
		A = sub_eig.buoyancy_eig_A(A, error, grid, m, Lmax, ind_eig, ind_eig_b, bc_i, bc_o, bc_i_temp, bc_o_temp, bc_i_chi, bc_o_chi, signRa * 10**(Ra), Ra2, gr, flag_temp)
		A = sc_sparse.csr_matrix(A)
		A = A_withoutRa + A
		IO.mmwrite('A.mtx', A)

		print('\t\tx Ra1 = %g, Ra2 = %g' % (signRa * 10**Ra, Ra2))
		start_time = timeit.default_timer()

		# Eigensolver call
		runcmd = 'python3 ' + abs_path_singe + '/singe_petsc_mpi.py ' + params.inline_slepc
		if params.nthreads > 1:
			runcmd = path_mpi + ' -n ' + str(params.nthreads) + ' ' + runcmd
		start_time = timeit.default_timer()
		os.system(runcmd)
		elapsed = timeit.default_timer() - start_time

		# Optim criterion
		eigval = np.loadtxt('Eigenval.txt')
		indmax = np.argmax(eigval[:,0])			# take the eigenvalue with the largest real
		sigma_c, omega_c = eigval[indmax,0], eigval[indmax,1]
		print('\t\t\t => Growth rate = %g  (elapsed %.4g s)' % (sigma_c, timeit.default_timer()-start_time))
		ra_cache[(Ra,Ra2)] = sigma_c

		optimarr = np.array([signRa * 10**Ra, Ra2, sigma_c, omega_c]).reshape(1,4)
		if sigma_c >= 0.0:
			if os.path.isfile('optimpos_iter.out') == True:
				optimarr = np.vstack([np.loadtxt('optimpos_iter.out'), optimarr])
			np.savetxt('optimpos_iter.out', optimarr)
		else:
			if os.path.isfile('optimneg_iter.out') == True:
				optimarr = np.vstack([np.loadtxt('optimneg_iter.out'), optimarr])
			np.savetxt('optimneg_iter.out',optimarr)

		return sigma_c

### assumes f is an increasing function of x.
def bracket_brentq(f, x1, x2=None, dx=0.3, tol=1e-6, maxiter=200, args=None):
	y1 = f(x1, *args)
	dx = abs(dx)		 # dx must be positive.
	if x2 is not None:   # check that we actually have a bracket
		y2 = f(x2, *args)
		if y2*y1 > 0:		# we don't have a bracket !!
			if (abs(y2) < abs(y1)):
				x1,y1 = x2,y2		# start from the value closest to the root
			x2 = None		# search needed.
	if x2 is None:		# search for a bracket
		x2 = x1
		if y1>0:  dx = -dx		# up or down ?
		while x2 > 1:
			x2 += dx
			y2 = f(x2, *args)
			if y2*y1 < 0: break
			x1,y1 = x2,y2
	if x2 <= 1.:
		return 0.0
	# Now that we know that the root is between x1 and x2, we use Brent's method:
	x0 = brentq(f, x1, x2, maxiter=maxiter, xtol=tol, rtol=tol, args=args)
	return x0

#-----------------------------------------------------------------------
# Radial grid
#-----------------------------------------------------------------------
if params.reg == 'reg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N)
	grid.mesh_reg()
elif params.reg == 'irreg':
	grid = sub_op.radial_grid(params.r0, params.rf, params.N, nin=params.nin, nout=params.nout)
	grid.mesh_irreg()

if mpi_rank == 0:
	print('o Radial grid')
	if params.reg == 'reg':
		print('\t* h = ' + str(grid.h))
	elif params.reg == 'irreg':
		print('\t* N = ' + str(grid.N))
		print('\t* hmin = ' + str(grid.dmin))
		print('\t* hmax = '+ str(grid.dmax))

comm.Barrier()

#-----------------------------------------------------------------------
# radial fields
#-----------------------------------------------------------------------
N2 = params.N2r(grid.r)
gr = params.gravity(grid.r)
if np.any(params.Rac):								# Can be single value or a list
	flag_temp = 2
	N2c = params.N2cr(grid.r)											# Radial field
	bc_i_chi = params.bc_i_chi
	bc_o_chi = params.bc_o_chi
else:
	flag_temp = 1
	N2c = np.zeros(np.size(grid.r))
	bc_i_chi = 1														# Default value. Does not play any role because composition is disabled!
	bc_o_chi = 1														# Idem

### parameter space (m,Rac) ####
mc_all = np.array(params.m)
Rac_all = np.ones((1,np.size(mc_all))) * np.array(params.Rac).reshape((-1,1))
mc_all = mc_all.reshape((1,-1)) * np.ones((np.size(params.Rac),1))

par_idx0 = mpi_rank * np.size(mc_all) // mpi_size
par_idx1 = (mpi_rank+1) * np.size(mc_all) // mpi_size
comm.Barrier()
print("\no Process %d optimising %d parameter sets (out of %d)" % (mpi_rank, par_idx1-par_idx0, np.size(mc_all)))
comm.Barrier()

#-----------------------------------------------------------------------
# Parallel loop over m and for Ra
#-----------------------------------------------------------------------
# Allocation on each process
mc_vec = mc_all.flat[par_idx0:par_idx1]			# m
Rac_vec = np.zeros(mc_vec.shape, dtype=float)	# First Ra to optimize (thermal)
Rac2_vec = Rac_all.flat[par_idx0:par_idx1]		# Second Ra (imposed)
omegac_vec = np.zeros(mc_vec.shape, dtype=float)   # frequency (to find)
sigmac_vec = np.zeros(mc_vec.shape, dtype=float)   # growth rate (to find)
count = 0

comm.Barrier()

# Do previous results exist ?
try:
	result_prev = np.loadtxt('m_Rac_omega_sigma_%d.txt' % mpi_rank)
	if (result_prev.shape == (mc_vec.size, 5)) and (np.all(mc_vec == result_prev[:,0])) and (np.allclose(Rac2_vec, result_prev[:,2])):
		Rac_vec = result_prev[:,1]
		omegac_vec = result_prev[:,3]
		sigmac_vec = result_prev[:,4]
		print('o Continuing previous job.')
	else:
		print('o Job has changed. Starting from scratch.')
except:
	pass

# Start value for finding the critical Rayleigh (in log-space)
#Ra_crit = np.log10(params.Ra)
signRa = np.sign(params.Ra)
Ra_crit = np.log10(np.abs(params.Ra))

for m, Rac in zip(mc_vec, Rac2_vec):
	if (Rac_vec[count] == 0.0) and (sigmac_vec[count] == 0.0) and (omegac_vec[count] == 0.0):		# compute only if not already computed
		m = int(m)
		print('\no Onset for fixed m = %d, Ra2 = %g' % (m,Rac))
		
		#----------------
		# Init
		#----------------
		path_mdir = 'm' + str(m)
		os.chdir(path_mdir)
		
		#----------------
		# Spectral scalar variables
		#----------------
		Lmax, ind_eig, ind_eig_b, sh = sub_eig.set_shtns(params.sym, params.Lmax, m)
		pol, tor, temp, chi, polb, torb = sub_eig.nb_eig_vec(m, params.N, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi)
		
		#----------------
		# Size of the matrices A and B
		#----------------
		if flag_temp == 1:
			nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec
		elif flag_temp == 2:
			nb_l = pol.nbel_vec + tor.nbel_vec + temp.nbel_vec + chi.nbel_vec
		
		#----------------
		# Eigenvalue optimization
		#----------------
		# Reading of A
		A_withoutRa = IO.mmread('A_withoutRa.mtx')
		A_withoutRa = sc_sparse.csr_matrix(A_withoutRa)

		# Find the zero from initial guess Ra_crit
		print('\t* Optimisation on Ra1')
		ra_cache = {}		# clear cache for optimize_Ra
		optargs=(A_withoutRa, nb_l, params.error, grid, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, Rac, gr, flag_temp)
		Ra_crit = bracket_brentq(optimize_Ra, Ra_crit, dx = dlogRa, tol=tol, maxiter=maxiter, args=optargs)
		dlogRa = 0.2	# for subsequent searches, we should use a lower value for dlogRa.

		# Storage and cleaning
		Rac_vec[count] = np.min(10**(Ra_crit)) * signRa
		#os.system('rm *.txt')    ## Dangerous, don't know if it is useful.
		
		# Onset marginal mode for each m
		print('\n\t* Eigenmode at the linear onset')
		os.system('rm Eigenval.txt')
		
		# Buoyancy force
		A = sc_sparse.dok_matrix((nb_l, nb_l), dtype=complex)
		A = sub_eig.buoyancy_eig_A(A, params.error, grid, m, Lmax, ind_eig, ind_eig_b, params.bc_i, params.bc_o, params.bc_i_temp, params.bc_o_temp, bc_i_chi, bc_o_chi, Rac_vec[count], Rac, gr, flag_temp)
		A = sc_sparse.csr_matrix(A)
		A = A + A_withoutRa
		IO.mmwrite('A.mtx', A)
		del A

		runcmd = 'python3 ' + abs_path_singe + '/singe_petsc_mpi.py ' + params.inline_slepc
		if params.nthreads > 1:
			runcmd = path_mpi + ' -n ' + str(params.nthreads) + ' ' + runcmd
		os.system(runcmd)

		# Load result of eigen solve:
		eigval = np.loadtxt('Eigenval.txt')
		indmin = np.abs(eigval[:,0]).argmin()
		omegac_vec[count] = eigval[indmin,1]
		sigmac_vec[count] = eigval[indmin,0]
		#os.system('rm *.mtx')
		os.rename('Real_Eigenvec.npy', 'Real_Eigenvec_Rac%.3g.npy' % Rac)	# keep mode for every Rac
		os.rename('Imag_Eigenvec.npy', 'Imag_Eigenvec_Rac%.3g.npy' % Rac)

		# Save result and print message
		os.chdir(CWD)
		result_all = np.transpose(np.vstack((mc_vec, Rac_vec, Rac2_vec, omegac_vec, sigmac_vec)))
		np.savetxt('m_Rac_omega_sigma_%d.txt' % mpi_rank, result_all)	# overwrite results
		print("\t\t=> Critical Rayleigh number for m = " + str(m) + " is Ra_crit = %.3e" %(signRa * 10**Ra_crit))
		print("\t\t=> Frequency at onset for m = " + str(m) + " is omega = %.3e" %(omegac_vec[count]))

	count += 1

comm.Barrier()

#-----------------------------------------------------------------------
# Gathering and save on process 0
#-----------------------------------------------------------------------
if mpi_rank == 0:
	result_all = np.loadtxt('m_Rac_omega_sigma_0.txt')
	for indmpi in range(1, mpi_size):
		resulti = np.loadtxt('m_Rac_omega_sigma_%d.txt' % indmpi)
		result_all = np.vstack((result_all, resulti))
	np.savetxt('m_Rac_omega_sigma.txt', result_all)	# overwrite results
	os.system('rm m_Rac_omega_sigma_*.txt')
	print("\nFinished. You may remove the matrices with 'rm m*/*.mtx'")
