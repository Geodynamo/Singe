%-----------------------------------------------------------------------
\chapter{Beginning}
\section{Description}
%-----------------------------------------------------------------------
\textbf{\sg} (Spherical INertia-Gravity Eigenmode) is a code computing linear eigenmodes of incompressible, stratified (double-diffusive) fluids enclosed in a spherical cavity (full sphere or spherical shell).
In addition to the Navier-Stokes equation, it takes into account the thermal and compositional equations in the Boussinesq framework.% and the coupled induction equation (in progress) with an imposed magnetic field.

\begin{verbatim}
#                   
#                        / _,\
#                        \_\
#             ,,,,    _,_)  #      /)
#            (= =)D__/    __/     //
#           C/^__)/     _(    ___//
#             \_,/  -.   '-._/,--'
#       _\\_,  /           -//.
#        \_ \_/  -,._ _     ) )
#          \/    /    )    / /
#          \-__,/    (    ( (
#                     \.__,-)\_
#                      )\_ / -(
#                     / -(////
#                    ////
# SINGE means monkey in French!
\end{verbatim}

\sg uses finite differences (second order) in the radial direction and a spherical harmonic decomposition (pseudo-spectral). \sg is written entirely in python\footnote{compatible with python 2.7.x and 3.x} with the built-in Numpy/Scipy packages. It also relies on the python implementation of the blazingly fast spherical harmonic transform library \href{https://bitbucket.org/nschaeff/shtns}{SHTns}. Finally, it also uses PETSc/SLEPc libraries (and their python wrappers petsc4py/slepc4py) to efficiently solve the generalised eigenvalue problem, using direct or iterative methods. 
Thus, \sg efficiently runs on your laptop or on supercomputers. 
A post-processing program, based on the one developed in the XSHELLS code, is provided to extract useful data and to export fields to Matplotlib or Paraview.

\sg is free software, distributed under the \href{http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html}{CeCILL Licence} (compatible with GNU GPL): everybody is free to use, modify and contribute to the code.

%-----------------------------------------------------------------------
\section{Requirements}
	\subsection{For \sg itself}
%-----------------------------------------------------------------------
\sg should work on any Unix-like system (like GNU/Linux or MacOS X). To run \sg, you have to to install: 
\begin{itemize}
	\item C++ and Fortran compilers with OpenMP support,
	\item the \href{http://www.mcs.anl.gov/petsc/}{PETSc library},
	\item the \href{http://slepc.upv.es/}{SLEPc library},
\end{itemize}
and the following Python environment
\begin{itemize}
	\item python 2.7.x or 3.x  including the scientific packages \href{http://www.numpy.org/}{Numpy}, \href{https://www.scipy.org/}{Scipy} and \href{https://pypi.python.org/pypi/mpi4py}{mpi4py},
	\item the \href{https://bitbucket.org/nschaeff/shtns}{SHTns library},
	\item the Python wrappers \href{https://bitbucket.org/petsc/petsc4py}{petsc4py} and \href{https://bitbucket.org/slepc/slepc4py}{slepc4py}.
\end{itemize}
%You may also install the symbolic package \href{http://www.sympy.org/fr/}{Sympy}, which will be mandatory for the future version (see later). 
Note that \sg has been written from scratch in python 3, but it is fully compatible with python 2.7.x thanks to the imports
\begin{verbatim}
	from __future__ import absolute_import
	from __future__ import division
	from __future__ import print_function
	from __future__ import unicode_literals
\end{verbatim}
at the beginning of each *.py file.

Finally, graphical outputs need 
\begin{itemize}
	\item the graphical package \href{http://matplotlib.org/}{matplotlib}, or \href{https://www.gnu.org/software/octave/}{GNU Octave},
	\item the \href{https://bitbucket.org/nschaeff/xshells}{\xs code} (xspp library). See the \xs manual in the same directory for further details. A better solution will be find later\dots 
\end{itemize}

The following item is not mandatory:
\begin{itemize}
	\item a shared MPI library (with thread support). You can also let petsc downloading its own MPI library (recommended for new users).
\end{itemize}

%-----------------------------------------------------------------------
\section{Installation}
%-----------------------------------------------------------------------
Installing the required Python environment should not be difficult on Unix-like systems. For instance for Debian users (also Ubuntu and cie, Linux Mint\dots){}
\begin{verbatim}
	sudo apt-get install python3 python3-numpy python3-scipy
\end{verbatim}

To install SHTNS with Python 3, the reader must refer to the webpage \url{https://users.isterre.fr/nschaeff/SHTns/python.html}

The tricky part is the installation of PETSc and SLEPc. In all cases, install first PETSc with complex scalars (mandatory because of the eigenvalue problem studied). Then it depends on the architecture of the computer that you are using\dots Please read the (exhaustive) documentation on the websites of PETSc and SLEPc. 

The installation process also depends on the external packages you want to use, such as SUPERLU DIST or MUMPS. Most of the external packages depend on other packages, which must be installed first. Note that some installation configurations are also incompatible, such as MUMPS with 64-bits integers. If you want to install such external libraries, it is recommended for new users to let PETSc downloading and installing them (to avoid any compatibility problem).

Please find below some configuration options which should work on personal laptops:
\begin{verbatim}
	# 32-bits with MUMPS, SuperLu, SuperLu_Dist
./configure --with-cc=gcc --with-cxx=g++ --with-fc=gfortran
--with-scalar-type=complex --with-mumps=1 --with-superlu=1 --with-superlu_dist=1 
--with-fortran-kernels=1 --with-debugging=no --download-mpich
--download-scalapack --download-metis --download-parmetis --download-mumps
--download-superlu --download-superlu_dist

	# 32-bits with SuperLu_Dist, SuperLu
./configure --with-cc=gcc --with-cxx=g++ --with-fc=gfortran
--with-scalar-type=complex --with-superlu=1 --with-superlu_dist=1
--download-mpich --download-scalapack --download-metis --download-parmetis
--download-superlu --download-superlu_dist

	# 64-bits with PASTIX
./configure --with-64-bit-indices=1 --with-cc=gcc --with-cxx=g++
--with-fc=gfortran --with-scalar-type=complex --with-ptscotch=1 --with-pastix=1
--download-mpich --download-scalapack --download-metis --download-parmetis
--download-ptscotch --download-pastix

	# 64-bits SuperLu_Dist
./configure --with-64-bit-indices=1 --with-cc=gcc --with-cxx=g++
--with-fc=gfortran --with-scalar-type=complex --with-superlu_dist=1
--with-fortran-kernels=1 --with-debugging=no --download-mpich
--download-scalapack --download-metis --download-parmetis  --download-superlu_dist

# SLEPc installation (same for 32 and 64-bits)
	# With arpack
./configure --with-arpack
make
make install

	# Otherwise
./configure
make
make install
\end{verbatim}
Note that the debugging mode was disabled with
\begin{verbatim}
	--with-fortran-kernels=1 --with-debugging=no
\end{verbatim}
for faster simulations.

%-----------------------------------------------------------------------
\section{Configuration file \texttt{params.py}}
%-----------------------------------------------------------------------
There is a single configuration file, called \texttt{params.py}. It is read by (almost) all of the scripts. Beware: the \texttt{params.py} file must not be saved in the same directory as the scripts, but in your working directory. Otherwise, the \texttt{params.py} located in the scripts' directory will be loaded by default at each simulation, even if \sg is launched from a different working directory with another \texttt{params.py}!

%-----------------------------------------------------------------------
\section{Running \sg}
%-----------------------------------------------------------------------
There are two running modes in \sg, one for free-eigenmodes computations and one to compute the onset of linear convection.
%-----------------------------------------------------------------------
	\subsection{Free eigenmodes}
%-----------------------------------------------------------------------
This mode has been developed for this study \citet{vidal2015quasi}. 
Here are the basic steps the user must follow to compute free eigenmodes with \sg.
\begin{enumerate}
	\item Adapt the \texttt{params.py} file and copy it in the working directory (different from the directory containing the scripts).
	\item Compute the $A$ and $B$ matrix (sequential) with the command
		\begin{verbatim}
			python path_to_script/singe_matrix_eig.py
		\end{verbatim}
	\item Call of the eigenvalue solver with the command-line \texttt{options} (see later)
		\begin{verbatim}
			python path_to_script/singe_petsc_mpi.py options
		\end{verbatim}
	\item Export into xshells format (sequential)
			\begin{verbatim}
			python path_to_script/singe_to_xshells.py
		\end{verbatim}
\end{enumerate}

%-----------------------------------------------------------------------
	\subsection{Thermal convection}
%-----------------------------------------------------------------------
This mode has been developed for this study \citet{kaplan2017subcritical}. 
Here are the steps the user must follow to compute onset of thermal or double-diffusive convection with \sg.
\begin{enumerate}
	\item Adapt the \texttt{params.py} file and copy it in the working directory (different from the directory containing the scripts).
	\item Compute the $A$ and $B$ matrix (sequential) with the command
		\begin{verbatim}
			python path_to_script/singe_matrix_conv.py
		\end{verbatim}
	\item Call of the eigenvalue solver with the command-line \texttt{options} (see later)
		\begin{verbatim}
			python path_to_script/singe_optom_conv.py options
		\end{verbatim}
	\item Export into xshells format (sequential)
			\begin{verbatim}
			python path_to_script/singe_to_xshells.py
		\end{verbatim}
\end{enumerate}

%-----------------------------------------------------------------------
	\subsection{SLEPc options}
%-----------------------------------------------------------------------
To call the SLEPc eigensolver, options must be specified by the user, such as the direct or implicit solver used, the linear algebra package used, the nature of the eigenvalue problem\dots For the moment, only a direct solver using a LU decomposition (sequential or parallel) has been tested. A complete description of these options can be found on the website of \href{http://slepc.upv.es/documentation/manual.htm}{SLEPc} and in the SLEPc user manual available on the website. 

In \texttt{params.py}, the following parameters must be specified:
\begin{itemize}
	\item \texttt{nev}: minimal number of eigenvalues to compute,
	\item \texttt{ncv}: proxy for the size of the Krylov subspace. \texttt{ncv} $\geq$ \texttt{2nev} at least. 10 times is a good value. Note that the higher \texttt{ncv} is, the more the problem is memory-bound and the more the cpu time increases.
	\item \texttt{eig}: part of the spectrum to compute.
	\begin{itemize}
		\item \texttt{'LM'}: Largest magnitude,
		\item \texttt{'SM'}: Smallest magnitude,
		\item \texttt{'LR'}: Largest real,
		\item \texttt{'SR'}: Smallest magnitude,
		\item \texttt{'LI'}: Largest imaginary,
		\item \texttt{'SI'}: Smallest imaginary,
		\item \texttt{'TM'}: Target magnitude,
		\item \texttt{'TR'}: Target real,
		\item \texttt{'TI'}: Target imaginary.
	\end{itemize}
	\item \texttt{$\tau$}: the target $\tau$,
	\item \texttt{tol}: tolerance of reject eigenvalues. $1e-12$ is the default value.
	\item \texttt{maxit}: maximum number of iterations.
\end{itemize}

\begin{example}
	\begin{verbatim}
		nev   = 10
		ncv   = 100
		eig   = 'TM'
		tau   = -10**(-3) - 0.015*1j
		tol   = 10**(-12)
		maxit = 40
	\end{verbatim}
\end{example}

Then the user must specify some command-line \texttt{options}. Here is an example which works pretty well for the case studied in Vidal \& Schaeffer (GJI,2015), using the shift and invert method with SUPERLU\_DIST (only solver available for 64-bits integers)
\begin{verbatim}
	-eps_balance oneside -eps_conv_abs -st_type sinvert 
	-st_pc_factor_mat_solver_type superlu_dist
\end{verbatim}

%-----------------------------------------------------------------------
\section{Outputs}
\label{sec:outputs}
%-----------------------------------------------------------------------
Whatever the file \texttt{params.py}, Here are the outputs generated by \sg:
\begin{itemize}
	\item \texttt{A.mtx} and \texttt{B.mtx} are the sparse matrices of the eigenvalue problem
	\begin{equation}
		A \mathbf{X} = \lambda B \mathbf{X},
	\end{equation}
	\item \texttt{Eigenval.txt}: list of the eigenvalues $\lambda$. The first column corresponds to the real part $\sigma$ and the second to the imaginary part $\omega$;
	\item \texttt{Real\_Eigenvec.npy} and \texttt{Imag\_Eigenvec.npy}: real and imaginary parts of the eigenvectors (temp files);
	\item \texttt{fieldX\#.out}: field X (velocity or temperature) corresponding to the \# eigenvalue in \texttt{Eigenval.txt} (\xs format).
\end{itemize}

%-----------------------------------------------------------------------
\section{Citing}
%-----------------------------------------------------------------------
If you use \sg for research work, you must cite at least these two articles
\begin{itemize}
	\item J. Vidal \& N. Schaeffer, \emph{Quasi-geostrophic modes in the Earth's fluid core with an outer stably stratified layer}, Geophys. J. Int. \textbf{202}, 2182--2193, \href{http://dx.doi.org/10.1093/gji/ggv282}{10.1093/gji/ggv282} (2015)
	\item N. Schaeffer, \emph{Efficient Spherical Harmonic Transforms aimed at pseudo-spectral numerical simulations}, Geochem. Geophys. Geosyst. \textbf{14}, 751-758, \href{http://dx.doi.org/10.1002/ggge.20071}{doi:10.1002/ggge.20071} (2013)
\end{itemize}
In addition, if you use \sg to determine the onset of convection, you must also cite \citet{kaplan2017subcritical}.

