\chapter{Post-processing with xspp}  \label{ch:xspp}

Fields are stored in binary files (see \ref{sec:outputs}), using the custom format developped for \xs. They can be handled after the simulation by the \texttt{xspp} command line program.


\section{Using the xspp command-line tool}

Compile the program by typing \texttt{make xspp}. Invoking it without arguments (by running \texttt{./xspp}) will print a help screen including the commands and their syntax.

\begin{example}
The following will display information about the file \texttt{fieldU.job} (resolution, precision, time of the snapshot, ...):
\begin{verbatim}
./xspp fieldU.job
\end{verbatim}

To compute the energy and maximum value of the curl of the field:
\begin{verbatim}
./xspp fieldU.job curl nrj max
\end{verbatim}

To extract the field values along a line spanning the $x$-axis from $x=-1$ to $x=0.8$, and also display total energy of field:
\begin{verbatim}
./xspp fieldU.job line -1,0,0 0.8,0,0 nrj
\end{verbatim}

Add two fields and save the result to a new file (the first file will set the resolution for the result):
\begin{verbatim}
./xspp fieldT_0004.job + fieldT0.job save fieldT_total_0004.job
\end{verbatim}

Extract only a given range of spherical harmonic coefficients (2 to 31) and computes the corresponding energy:
\begin{verbatim}
./xspp fieldB.job llim 2:31 nrj
\end{verbatim}

\end{example}

Note that \texttt{xspp} is not parallelized using MPI, so that for very big cases you might run out of memory (although it can operate out-of-core -- without actually loading the whole file in memory -- in some cases).
As a workaround you can always reduce the spherical harmonic truncation while reading your big files with the \texttt{llim} option (see example avobe).

 
\section{Extract and plot 2D slices}

One of the most common usage for \texttt{xspp} is to extract two-dimensional slices of the 3D data stored in spectral representation in the field files.
Four types of 2D slices are available:
\begin{itemize}
\item Meridian cuts (a plane containing the $z$-axis), with the \texttt{merid} command;
\item Equatorial cuts (the plane $z=0$), with the \texttt{equat} command;
\item Surface data (on a sphere of given radius $r$), with the \texttt{surf} command;
\item Disc cuts (an arbitrary plane), with the \texttt{disc} command;
\end{itemize}
When these commands are given to \texttt{xspp}, it will write text files corresponding to the required cuts. They can then be loaded and displayed
using matlab, octave or python with matplotlib (see next sections).

\begin{example}
A meridian cut at $\phi = 0$:
\begin{verbatim}
./xspp fieldU.job merid
\end{verbatim}

An equatorial cut, and a meridian cut at $\phi=45$degrees, of the vorticity (curl of U)
\begin{verbatim}
./xspp fieldU.job curl equat merid 45
\end{verbatim}

Extract the field at the spherical surface closest to $r=0.9$, using only the symmetric components.
\begin{verbatim}
./xspp fieldU.job sym 0 surf 0.9
\end{verbatim}

Make a cut at $z=0.7$, using 200 azimuthal points, with field truncated at harmonic degree 60:
\begin{verbatim}
./xspp fieldU.job llim 0:60 disc 200 0,0,0.7
\end{verbatim}

\end{example}


\subsection{plotting with matlab/octave}

Matlab or Octave scripts are located in the \texttt{matlab} dierectory.
There are scripts to load and plot cuts obtained with \texttt{xspp}.

\begin{example}
Produce a meridian cut with \texttt{xspp}:
\begin{verbatim}
./xspp fieldU.job merid
\end{verbatim}

Then, from octave (or matlab), load and plot the $\phi$-component of the field in this meridional slice:
\begin{verbatim}
> [Vphi,r,theta] = load_merid('o_Vp.0');
> plot_merid(r,theta,Vphi)
\end{verbatim}

\end{example}


\subsection{plotting with python/matplotlib}

The python module \texttt{xsplot} is provided to load and display cuts produced by \texttt{xspp}.
It can be used interactively or within scripts.
Such Python scripts using matplotlib and xsplot are located in the \texttt{matplotlib} dierectory,
and can be called from command line.
\texttt{xsplot} can also be used directly from command line and will guess the type of cut of your file and display it accordingly.

The python module should be installed by calling \texttt{make install}, or explicitly \texttt{python setup.py install --user} from the \texttt{python} subdirectory, to install it for the current user only. 

\begin{example}
Produce a meridian and an equatorial cut with \texttt{xspp}:
\begin{verbatim}
./xspp fieldU.job merid equat
\end{verbatim}

From command prompt, quickly load and plot all three components in cylindrical coordinates of the field in this meridional slice, as well as in the equatorial plane.
\begin{verbatim}
xsplot o_Vs.0 o_Vp.0 o_Vz.0 o_equat
\end{verbatim}

Alternatively, from an Ipython interpreter (or notebook, or script), load and plot the $\phi$-component of the field in the meridional and equatorial slices:
\begin{verbatim}
> import xsplot
> r,theta,Vphi = xsplot.load_merid('o_Vp.0')
> xsplot.plot_merid(r,theta,Vphi)
> s,phi,Vs,Vp,Vz = xsplot.load_disc('o_equat')
> xsplot.plot_disc(s,phi, Vp)
\end{verbatim}

\end{example}


\section{3D visualization with paraview}

From the \href{http://www.paraview.org}{paraview} website:
\textit{ParaView is an open-source, multi-platform data analysis and visualization application. ParaView users can quickly build visualizations to analyze their data using qualitative and quantitative techniques.}

Full spatial fields can be saved to XDMF format, which can be loaded by paraview.
Note that the HDF5 library is required for this to work, and must be found by the \texttt{configure} script.
If so, Simply run:

\begin{verbatim}
make xspp
./xspp fieldB_0004.job hdf5 B_cartesian.h5
\end{verbatim}

The file \texttt{B\_cartesian.h5.xdmf} describes the cartesian components of the vector field B on a spherical grid that can be read directly by paraview (if prompted for a loader, select 'XDMF').


\section{Advanced post-processing using pyxshells}

For more complex post-processing, xspp may not be enough.
The python module \texttt{pyxshells} allows you to quickly write your own scripts to work directly with the spectral fields stored in the \texttt{field} files output by \sg, cast them to spatial domain, and so on...
