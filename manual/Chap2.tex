\chapter{Setting up the simulation with \texttt{params.py}}
\label{setup}

The file \texttt{params.par} is a Python file used as a parameter file. Example configuration files can be found in the \texttt{examples} directory.

%-----------------------------------------------------------------------
	\section{Mathematical modelling}
		\subsection{Governing equations}
%-----------------------------------------------------------------------
\sg can solves the eigenvalue-eigenmode pairs of the  Navier-Stokes equation in a rotating reference frame.
Optionally it includes a radial buoyancy force in the Boussinesq approximation, for an arbitrary radial gravity field $g(r)$ in the double-diffusive regime. Precisely, the equations solved by \sg are
\begin{subequations}
\begin{align}
  \lambda \boldsymbol{u} + 2 \, \Omega_{0} \left ( \boldsymbol{1}_z \times \boldsymbol{u} \right ) &= -\nabla p + \nu \nabla^{2} \boldsymbol{u} + (Ra_T \, \Theta + Ra_c \, \xi ) g(r) \, \boldsymbol{1}_r, \label{eq_u} \\
  \lambda \Theta + N_T^{2}(r) \, u_{r} &= \kappa_T \nabla^{2} \Theta, \label{eq_t} \\
  \lambda \xi + N_C^{2}(r) \, u_{r} &= \kappa_C \nabla^{2} \xi, \label{eq_c} \\
  \nabla \cdot \boldsymbol{u} &= 0,
\end{align}
\end{subequations}
where
\begin{itemize}
	\item $\lambda$ is the eigenvalue (see later),
	\item $\boldsymbol{u}$ is the velocity field,
	\item $\Theta$ is the temperature scalar in the Boussinesq formulation,
	\item $\xi$ is the compositional scalar in the Boussinesq formulation,
	\item $\nu$ is the kinematic viscosity of the fluid and is set by the variable \texttt{nu} in \texttt{params.py};
	\item $\kappa_T$ is the thermal diffusivity of the fluid and is set by the variable \texttt{kappa} in \texttt{params.py};
	\item $\kappa_C$ is the thermal diffusivity of the fluid and is set by the variable \texttt{kappac} in \texttt{params.py};
	\item $\Omega_0$ is the amplitude of the rotation vector of the rotating reference frame, along the vertical unitary axis $\boldsymbol{1}_z$. It is set by the variable \texttt{Omega0} in \texttt{params.py};
	\item $N_T^{2}(r) = {\mathrm{d} T_{0}}/{\mathrm{d}r}$ is the thermal Brunt-Väisälä frequency depending on the imposed base scalar profile $T_{0}(r)$. In \texttt{params.py}, it is defined by a function \texttt{def function N2r(r)} depending of the radial variable $r$.
	\item $N_C^{2}(r) = {\mathrm{d} C_{0}}/{\mathrm{d}r}$ is the compositional Brunt-Väisälä frequency depending on the imposed base composition profile $C_{0}(r)$. In \texttt{params.py}, it is defined by a function \texttt{def function N2cr(r)} depending of the radial variable $r$.
	\item $p$ is the dynamic reduced pressure.
\end{itemize}
Note that it is up to the user to choose dimensional or non-dimensional control parameters.

To disable the thermal and compositional equations, set \texttt{Ra=0}, \texttt{kappa=0}, \texttt{Rac=0}, \texttt{kappac=0} in \texttt{params.py}. To disable only the compositional equation, \texttt{Rac=0}, \texttt{kappac=0} in \texttt{params.py}. By definition, the first scalar $\Theta$ is the one for which the Rayleigh number is optimised. Hence, if you want to optimise other the second scalar, invert the effet of (\texttt{Ra=0}, \texttt{kappa=0}) and (\texttt{Rac=0}, \texttt{kappac=0}) in \texttt{params.py}.

%-----------------------------------------------------------------------
	\subsection{Boundary conditions}
	\label{sec:bc}
%-----------------------------------------------------------------------
\paragraph{Scalars $\Theta$ and $\xi$} Boundary conditions are either fixed values
\begin{equation}
	\Theta = 0, \ \, \ \xi = 0,
\end{equation}
or or fixed fluxes
\begin{equation}
	\frac{\partial \Theta}{\partial r} = 0,  \ \, \ \frac{\partial \xi}{\partial r} = 0.
\end{equation}

\paragraph{Velocity.} For the full sphere, the velocity field satisfies a regularity boundary condition at the centre \citep{lewis1990physical}, imposed on the poloidal and toroidal scalars. At a shell boundary, the velocity field always satisfies the impermeability condition
\begin{equation}
	\boldsymbol{u} \cdot \boldsymbol{1}_r = 0.
\end{equation}
Then, it must satisfy either the stress-free condition
\begin{equation}
	\boldsymbol{1}_r  \times \left ( \overline{\overline{\sigma}} \cdot \boldsymbol{1}_r  \right ) = \boldsymbol{0},
\end{equation}
where $\overline{\overline{\sigma}}$ is the stress tensor, or the no-slip boundary condition
\begin{equation}
	\boldsymbol{u} \times \boldsymbol{1}_r  = \boldsymbol{0}.
\end{equation}

The inner and outer boundary conditions are set in \texttt{params.py} and allow to select independently the appropriate boundary conditions:
\begin{itemize}
	\item \texttt{bc\_o} and \texttt{bc\_o} for the velocity field. \texttt{bc\_i = 0} for the full sphere, \texttt{bc\_i = 1} for stress free and \texttt{bc\_i = 2} for no-slip. BC at the outer boundary are similar.
	\item \texttt{bc\_i\_temp} and \texttt{bc\_o\_temp} for the temperature field. \texttt{bc\_i\_temp = 0} for the full sphere, \texttt{bc\_i\_temp = 1} for fixed flux and \texttt{bc\_i\_temp = 2} for imposed temperature. BC at the outer boundary are similar.
	\item \texttt{bc\_i\_chi} and \texttt{bc\_o\_chi} for the compositional field. \texttt{bc\_i\_chi = 0} for the full sphere, \texttt{bc\_i\_chi = 1} for fixed flux and \texttt{bc\_i\_chi = 2} for imposed temperature. BC at the outer boundary are similar.
\end{itemize}

%\begin{example}
%	The following lines in \texttt{xshells.par} define
%	zero velocity and fixed temperature boundary condition at the inner boundary, and no-slip and fixed flux boundary condition at the outer boudnary.
%	\begin{verbatim}
%		BC_U = 0,1    # inner,outer boundary conditions 
%					  # (0=zero velocity, 1=no-slip, 2=free-slip)
%		BC_T = 1,2    # 1=fixed temperature, 2=fixed flux.
%	\end{verbatim}
%\end{example}

%-----------------------------------------------------------------------
	\subsection{Eigenvalue problem}
	\label{sec:eig}
%-----------------------------------------------------------------------
The governing equations are recast as an generalised eigenvalue problem
\begin{equation}
	\mathcal{A} \boldsymbol{X} = \lambda \mathcal{B} \boldsymbol{X},
	\label{Eq_GNHEP}
\end{equation}
with $\mathcal{A}$ and $\mathcal{B}$ two linear operators and the eigenvalue
\begin{equation}
	\lambda = \sigma + \mathrm{i} \, \omega,
\end{equation}
where $\sigma$ is the damping rate and $\omega$ the frequency of the eigenmode.

Following \citet{rieutord1997inertial} and \citet{dintrans1999gravito}, the original eigenvalue problem (\ref{Eq_GNHEP}) is not solved directly by \sg. Instead, a spectral transform is applied, namely the shift and invert method. Hence, eigenvalue problem (\ref{Eq_GNHEP}) is converted into the eigenvalue problem
\begin{equation}
	(\mathcal{A} - \tau \mathcal{B})^{-1} \mathcal{B} \boldsymbol{X} = \mu \boldsymbol{X},
	\label{Eq_Shift_Invert}
\end{equation}
where $\tau$ is the target eigenvalue and $\mu$ the new eigenvalue linked to $\lambda$ by
\begin{equation}
	\mu = \frac{1}{\lambda - \tau}.
\end{equation}
This transformation is effective for finding eigenvalues near $\tau$, since the eigenvalues $\mu$ of the operator that are largest in magnitude correspond to the eigenvalues $\lambda$ of the original problem that are closest to the shift $\tau$ in absolute value. Note that the eigenvectors remain unchanged.

Note that there are other spectral transforms, such as the Cayley transform, which can be used with the corresponding command-line option. Please refer to the SLEPc manual for further details.

%-----------------------------------------------------------------------
\section{Numerical modelling}
	\subsection{Internal representation of vector fields}
%-----------------------------------------------------------------------
Vector fields are represented internally using a poloidal/toroidal decomposition
\begin{equation}
\boldsymbol{u} = \nabla \times (T r \boldsymbol{1}_r) + \nabla \times \nabla \times (P r \boldsymbol{1}_r),
\end{equation}
where $\boldsymbol{1}_r$ is the unit radial vector, and $T$ and $P$ are the toroidal and poloidal scalars respectively.
This decomposition ensures that the vector field $\boldsymbol{u}$ is divergence-free. 

The scalar fields $T$ and $P$ for each radial shell are then decomposed on the basis of spherical harmonics. For that, the velocity and temperature fields are separated according to their equatorial symmetry:
\begin{itemize}
	\item equatorially symmetric field such that
	\begin{subequations}
	\begin{align}
		\left [ u_r, u_\theta, u_\phi \right ] (r, \theta, \phi) &= \left [ u_r, -u_\theta, u_\phi \right ] (r, \pi - \theta, \phi), \\
		\Theta (r, \theta, \phi) &= \Theta (r, \pi-\theta, \phi).
	\end{align}
	\end{subequations}
	This symmetry is enabled with \texttt{sym='pos'} in \texttt{params.py}.
	\item antiequatorially symmetric field such that
	\begin{subequations}
	\begin{align}
		\left [ u_r, u_\theta, u_\phi \right ] (r, \theta, \phi) &= \left [ -u_r, u_\theta, -u_\phi \right ] (r, \pi - \theta, \phi), \\
		\Theta (r, \theta, \phi) &= -\Theta (r, \pi-\theta, \phi).
	\end{align}
	\end{subequations}
	This symmetry is enabled with \texttt{sym='neg'} in \texttt{params.py}.
\end{itemize}

%-----------------------------------------------------------------------
	\subection{Radial grid}
%-----------------------------------------------------------------------
As its father \xs, \sg uses second order finite differences in radius.
The total number of radial grid intervals (number of points - 1) is defined in \texttt{params.py} by the variable \texttt{N}.
The radial extent of both velocity and temperature fields is set using \texttt{r0} and \texttt{rf} variables, determing the radius of the first and last shells. The \texttt{NR} grid points will be distributed between radii corresponding to the minimum and maximum of these values. \texttt{reg} must be equal to \texttt{'irreg'} (resp. \texttt{'reg'}) for an irregular (resp. a regular) radial mesh.

The irregular grid(recommanded choice) refines the number of points in the boundary layers, and this refinement can be controlled by the two variables \texttt{nin} and \texttt{nout}, the first and the second being the number of points reserved for the inner and outer boundary layer respectively, reinforcing the normal refinement.

\begin{example}
	The following lines in \texttt{params.py} define a grid from 0 to 1, with a total of 241 radial grid points, with 10 and 5 points reserved to the refinement of the inner and outer boundary layer respectively.
	\begin{verbatim}
		reg   = 'irreg'
		r0    = 0
		rf    = 1
		error = 2
		N     = 240
		nin   = 10
		nout  = 5
	\end{verbatim}
\end{example}

In the case of no-slip boundary condition, it is necessary to have enough points in the boundary layer not to induced a wrong Ekman pumping in the bulk. Following Dormy (PhD thesis, 1997), 10 points in the BL is a sufficient condition. To ensure it, the user may adjust \texttt{nin} and \texttt{nout}. A good (maybe too restrictive) condition is to choose them such that the variable \texttt{hmin} (which is printed in the terminal) is 10 times smaller than $\sqrt{E}$, where $E$ is the Ekman number.

%-----------------------------------------------------------------------
	\subsection{Angular grid and spherical harmonic truncation}
%-----------------------------------------------------------------------
\sg uses spherical harmonics to represent fields of a given azimutal symmetry $m$ in the spherical volume
\begin{equation}
	f(r, \theta,\phi) = \sum_{\ell=m}^{L}  f_\ell^{m} (r) Y_\ell^{m}(\theta, \phi) 
\end{equation}
where $Y_\ell^m$ is the spherical harmonic of degree $\ell$ and order $m$.
Numerically, the expansion stores only $m=0$ and the given $m$ in longitude, with a truncation of the spherical harmonic degree at maximum degree \texttt{Lmax}. In \texttt{params.py}, \texttt{Lmax} is arbitrary but such that \texttt{Lmax} > \texttt{m}. Note that the true \texttt{Lmax} is modified by \sg and may be \texttt{Lmax} $\pm$ 1, in order to have the same number of unknowns for poloidal and toroidal scalars satisfying the equatorial symmetry condition specified by the user.

The angular grid (spanning the co-latitude $\theta$ and longitude $\phi$) consists of \texttt{nphi} regularly spaced points in longitude, and \texttt{nlat} gauss nodes in latitude which are internally specified by SHTNS \citep{schaeffer2013efficient}.

Finally, note that only half of the number of spherical harmonic degrees is actually stored, because of the chosen equatorial symmetry.

