# SINGE
is a solver for Spherical INertia-Gravity Eigenmodes, written in Python

Copyright (c) 2014-2018 Université Grenoble Alpes.
written by Dr Jérémie VIDAL (UGA, Grenoble, France).
SINGE is distributed under the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible) located in the LICENSE file.

![inertial mode](examples/Eigenmodes/Rieutord1997/mode_w0.66277_small.jpg)


FEATURES:
---------

SINGE is a Python 2/3 code. It computes, for full spheres and spherical shells,
inertial and inertia-gravito modes in the mantle frame of reference.
Boussinesq, homegeneous and viscous fluids are taken into account,
with various different boundary conditions (no slip / stress-free
for the velocity field, constant heat flux / isothermal for the temperature).

It uses a parallel pseudo-spectral approach in spherical geometry.
The velocity field is projetcted onto poloidal and toroidal scalars,
which are expanded on spherical harmonics in the angular directions and
finite differences on an irregular mesh in the radial direction.

SINGE relies on other open-source libraries: 

- [SHTns](https://bitbucket.org/nschaeff/shtns) to compute spherical harmonics,
- [PETSc](http://www.mcs.anl.gov/petsc/) and [SLEPc](http://www.grycap.upv.es/slepc/) to solve the eigenvalue problem in parallel (with MPI).


DOCUMENTATION:
--------------

- A quickstart guide can be found below.
- A user manual is available in the `manual` directory.
- A related research paper has been published:
  [Quasi-geostrophic modes in the Earth's fluid core with an outer stably stratified layer](http://dx.doi.org/10.1093/gji/ggv282),
  also [available from arXiv](http://arxiv.org/abs/1501.07006).
- If you use SINGE for research work, please **cite this paper** (and maybe also papers related to SLEPc and SHTns):

		@article{vidal2015,
		  title={Quasi-geostrophic modes in the Earth's fluid core with an outer stably stratified layer},
		  author={Vidal, J{\'e}r{\'e}mie and Schaeffer, Nathana{\"e}l},
		  journal={Geophysical Journal International},
		  doi={10.1093/gji/ggv282},
		  volume={202}, number={3}, pages={2182--2193},
		  year={2015},
		}

INSTALL:
--------

First, ensure you have Python 2/3 installed. Then:

- [Download SHTns](https://bitbucket.org/nschaeff/shtns/downloads) and install it with the Python interface.

		./configure --enable-python --disable-openmp
		make
		python setup.py install --user

- [Download PETSc](http://www.mcs.anl.gov/petsc/download/index.html) and install it,
  using complex artithmetic, and optionally MUMPS (please refer to the PETSc documentation):

./configure --CFLAGS="-O3 -march=native -ffast-math" --FFLAGS="-O3 -march=native -ffast-math" --LDFLAGS="-O3" --with-scalar-type=complex --with-mpi=1 --with-blacs=1 --download-metis --download-parmetis --with-fortran-kernels=1 --with-debugging=no --with-mumps=1 --download-mumps --with-superlu-dist --download-superlu-dist --with-scalapack=1 --download-scalapack CXXOPTFLAGS="-O3 -ffast-math -march=native" --CXXFLAGS="-O3 -march=native -ffast-math" 
		make all test

- [Download SLEPc](http://www.grycap.upv.es/slepc/download/download.htm) and install it:

		./configure
		make

- Install the following python packages:
  numpy, [mpi4py](https://bitbucket.org/mpi4py), [petsc4py](https://bitbucket.org/petsc/petsc4py),
  [slepc4py](https://bitbucket.org/slepc/slepc4py).

- For plotting, you may find convenient to use the `xspp` post-processing program
  (part of the [XSHELLS](https://bitbucket.org/nschaeff/xshells) code), although the `pyxshells.py`
  module included in SINGE can do the job.
  You will probably also need matplotlib or one of the following software: matplotlib, octave, matlab, paraview.


RUNNING:
--------

To run SINGE,  please refer to the user manual (located in the `manual` directory).
The `examples` directory contains a few sample `params.py` files for solving various problems.
